* Project:       sigma
* File content:  overview of actual water and solute balance components
* File name:     Result.bal
* Model version: Swap 3.2.89
* Generated at:  24-Nov-2017 16:43:54

Period             :  01-Jan-1989 until  31-Dec-1989
Depth soil profile :  550.00 cm

            Water storage
Final   :       181.56 cm
Initial :       178.60 cm
            =============
Change            2.96 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    93.98    Interception      :     0.05
Runon          :     0.00    Runoff            :    29.34
Irrigation     :     0.00    Transpiration     :     4.71
Bottom flux    :    -0.03    Soil evaporation  :    56.90
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    93.95    Sum               :    91.00

Period             :  01-Jan-1990 until  31-Dec-1990
Depth soil profile :  550.00 cm

            Water storage
Final   :       181.18 cm
Initial :       181.56 cm
            =============
Change           -0.37 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   127.25    Interception      :     0.92
Runon          :     0.00    Runoff            :    45.68
Irrigation     :     0.00    Transpiration     :    22.75
Bottom flux    :    -0.98    Soil evaporation  :    57.29
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   126.27    Sum               :   126.64

Period             :  01-Jan-1991 until  31-Dec-1991
Depth soil profile :  550.00 cm

            Water storage
Final   :       190.75 cm
Initial :       181.18 cm
            =============
Change            9.57 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   119.75    Interception      :     0.66
Runon          :     0.00    Runoff            :    30.93
Irrigation     :     0.00    Transpiration     :    20.61
Bottom flux    :    -0.47    Soil evaporation  :    57.52
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   119.28    Sum               :   109.72

Period             :  01-Jan-1992 until  31-Dec-1992
Depth soil profile :  550.00 cm

            Water storage
Final   :       182.98 cm
Initial :       190.75 cm
            =============
Change           -7.77 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    96.92    Interception      :     0.49
Runon          :     0.00    Runoff            :    22.19
Irrigation     :     0.00    Transpiration     :    22.92
Bottom flux    :    -1.71    Soil evaporation  :    57.38
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    95.21    Sum               :   102.99

Period             :  01-Jan-1993 until  31-Dec-1993
Depth soil profile :  550.00 cm

            Water storage
Final   :       196.32 cm
Initial :       182.98 cm
            =============
Change           13.34 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   143.73    Interception      :     2.85
Runon          :     0.00    Runoff            :    39.45
Irrigation     :     0.00    Transpiration     :    27.58
Bottom flux    :    -1.54    Soil evaporation  :    58.98
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   142.19    Sum               :   128.85

Period             :  01-Jan-1994 until  31-Dec-1994
Depth soil profile :  550.00 cm

            Water storage
Final   :       180.71 cm
Initial :       196.32 cm
            =============
Change          -15.61 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    99.27    Interception      :     1.12
Runon          :     0.00    Runoff            :    24.22
Irrigation     :     0.00    Transpiration     :    26.88
Bottom flux    :    -8.73    Soil evaporation  :    53.93
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    90.54    Sum               :   106.15

Period             :  01-Jan-1995 until  31-Dec-1995
Depth soil profile :  550.00 cm

            Water storage
Final   :       175.77 cm
Initial :       180.71 cm
            =============
Change           -4.93 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    87.79    Interception      :     0.55
Runon          :     0.00    Runoff            :    19.96
Irrigation     :     0.00    Transpiration     :    21.11
Bottom flux    :    -1.31    Soil evaporation  :    49.79
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    86.48    Sum               :    91.41

Period             :  01-Jan-1996 until  31-Dec-1996
Depth soil profile :  550.00 cm

            Water storage
Final   :       170.78 cm
Initial :       175.77 cm
            =============
Change           -5.00 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    84.82    Interception      :     0.59
Runon          :     0.00    Runoff            :    16.41
Irrigation     :     0.00    Transpiration     :    19.52
Bottom flux    :     0.15    Soil evaporation  :    53.45
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    84.97    Sum               :    89.96

Period             :  01-Jan-1997 until  31-Dec-1997
Depth soil profile :  550.00 cm

            Water storage
Final   :       179.67 cm
Initial :       170.78 cm
            =============
Change            8.89 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   111.63    Interception      :     1.19
Runon          :     0.00    Runoff            :    22.86
Irrigation     :     0.00    Transpiration     :    19.60
Bottom flux    :     1.01    Soil evaporation  :    60.10
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   112.64    Sum               :   103.75

Period             :  01-Jan-1998 until  31-Dec-1998
Depth soil profile :  550.00 cm

            Water storage
Final   :       172.52 cm
Initial :       179.67 cm
            =============
Change           -7.15 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    89.64    Interception      :     1.98
Runon          :     0.00    Runoff            :    16.22
Irrigation     :     0.00    Transpiration     :    34.67
Bottom flux    :     1.35    Soil evaporation  :    45.27
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    90.99    Sum               :    98.14

Period             :  01-Jan-1999 until  31-Dec-1999
Depth soil profile :  550.00 cm

            Water storage
Final   :       170.83 cm
Initial :       172.52 cm
            =============
Change           -1.69 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    96.02    Interception      :     1.50
Runon          :     0.00    Runoff            :    21.07
Irrigation     :     0.00    Transpiration     :    29.48
Bottom flux    :     1.49    Soil evaporation  :    47.15
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    97.51    Sum               :    99.20

Period             :  01-Jan-2000 until  31-Dec-2000
Depth soil profile :  550.00 cm

            Water storage
Final   :       189.77 cm
Initial :       170.83 cm
            =============
Change           18.95 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   131.90    Interception      :     0.67
Runon          :     0.00    Runoff            :    34.61
Irrigation     :     0.00    Transpiration     :    16.56
Bottom flux    :     1.21    Soil evaporation  :    62.33
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   133.11    Sum               :   114.17

Period             :  01-Jan-2001 until  31-Dec-2001
Depth soil profile :  550.00 cm

            Water storage
Final   :       192.21 cm
Initial :       189.77 cm
            =============
Change            2.43 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   143.65    Interception      :     2.24
Runon          :     0.00    Runoff            :    43.49
Irrigation     :     0.00    Transpiration     :    31.62
Bottom flux    :    -5.36    Soil evaporation  :    58.52
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   138.29    Sum               :   135.86

Period             :  01-Jan-2002 until  31-Dec-2002
Depth soil profile :  550.00 cm

            Water storage
Final   :       193.06 cm
Initial :       192.21 cm
            =============
Change            0.85 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   142.60    Interception      :     1.45
Runon          :     0.00    Runoff            :    42.85
Irrigation     :     0.00    Transpiration     :    22.30
Bottom flux    :    -7.20    Soil evaporation  :    67.96
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   135.40    Sum               :   134.55

Period             :  01-Jan-2003 until  31-Dec-2003
Depth soil profile :  550.00 cm

            Water storage
Final   :       192.53 cm
Initial :       193.06 cm
            =============
Change           -0.52 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   133.42    Interception      :     2.33
Runon          :     0.00    Runoff            :    30.19
Irrigation     :     0.00    Transpiration     :    32.75
Bottom flux    :    -8.13    Soil evaporation  :    60.54
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   125.29    Sum               :   125.81

Period             :  01-Jan-2004 until  31-Dec-2004
Depth soil profile :  550.00 cm

            Water storage
Final   :       181.37 cm
Initial :       192.53 cm
            =============
Change          -11.16 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    98.26    Interception      :     1.20
Runon          :     0.00    Runoff            :    14.28
Irrigation     :     0.00    Transpiration     :    28.88
Bottom flux    :    -6.32    Soil evaporation  :    58.74
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    91.94    Sum               :   103.10

Period             :  01-Jan-2005 until  31-Dec-2005
Depth soil profile :  550.00 cm

            Water storage
Final   :       173.38 cm
Initial :       181.37 cm
            =============
Change           -7.99 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    93.76    Interception      :     1.84
Runon          :     0.00    Runoff            :    19.24
Irrigation     :     0.00    Transpiration     :    32.42
Bottom flux    :    -0.52    Soil evaporation  :    47.73
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    93.24    Sum               :   101.23

Period             :  01-Jan-2006 until  31-Dec-2006
Depth soil profile :  550.00 cm

            Water storage
Final   :       180.37 cm
Initial :       173.38 cm
            =============
Change            7.00 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   123.11    Interception      :     1.28
Runon          :     0.00    Runoff            :    23.90
Irrigation     :     0.00    Transpiration     :    23.25
Bottom flux    :     0.64    Soil evaporation  :    68.33
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   123.75    Sum               :   116.75

Period             :  01-Jan-2007 until  31-Dec-2007
Depth soil profile :  550.00 cm

            Water storage
Final   :       183.00 cm
Initial :       180.37 cm
            =============
Change            2.63 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   116.36    Interception      :     1.79
Runon          :     0.00    Runoff            :    30.93
Irrigation     :     0.00    Transpiration     :    29.60
Bottom flux    :     0.98    Soil evaporation  :    52.40
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   117.34    Sum               :   114.71

Period             :  01-Jan-2008 until  31-Dec-2008
Depth soil profile :  550.00 cm

            Water storage
Final   :       174.19 cm
Initial :       183.00 cm
            =============
Change           -8.81 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    89.80    Interception      :     1.48
Runon          :     0.00    Runoff            :    19.35
Irrigation     :     0.00    Transpiration     :    29.91
Bottom flux    :    -1.93    Soil evaporation  :    45.95
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    87.87    Sum               :    96.68

Period             :  01-Jan-2009 until  31-Dec-2009
Depth soil profile :  550.00 cm

            Water storage
Final   :       186.53 cm
Initial :       174.19 cm
            =============
Change           12.34 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   130.31    Interception      :     0.58
Runon          :     0.00    Runoff            :    30.50
Irrigation     :     0.00    Transpiration     :    12.15
Bottom flux    :     0.29    Soil evaporation  :    75.03
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   130.60    Sum               :   118.25

Period             :  01-Jan-2010 until  31-Dec-2010
Depth soil profile :  550.00 cm

            Water storage
Final   :       174.18 cm
Initial :       186.53 cm
            =============
Change          -12.35 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   101.15    Interception      :     1.77
Runon          :     0.00    Runoff            :    31.86
Irrigation     :     0.00    Transpiration     :    33.20
Bottom flux    :     0.00    Soil evaporation  :    46.67
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   101.15    Sum               :   113.50

Period             :  01-Jan-2011 until  31-Dec-2011
Depth soil profile :  550.00 cm

            Water storage
Final   :       172.72 cm
Initial :       174.18 cm
            =============
Change           -1.46 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    86.07    Interception      :     0.26
Runon          :     0.00    Runoff            :    16.84
Irrigation     :     0.00    Transpiration     :    17.08
Bottom flux    :     0.30    Soil evaporation  :    53.64
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    86.37    Sum               :    87.82

Period             :  01-Jan-2012 until  31-Dec-2012
Depth soil profile :  550.00 cm

            Water storage
Final   :       194.67 cm
Initial :       172.72 cm
            =============
Change           21.94 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   156.84    Interception      :     1.03
Runon          :     0.00    Runoff            :    47.07
Irrigation     :     0.00    Transpiration     :    18.38
Bottom flux    :     0.90    Soil evaporation  :    69.32
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   157.74    Sum               :   135.79

Period             :  01-Jan-2013 until  31-Dec-2013
Depth soil profile :  550.00 cm

            Water storage
Final   :       178.89 cm
Initial :       194.67 cm
            =============
Change          -15.77 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   102.28    Interception      :     1.34
Runon          :     0.00    Runoff            :    25.90
Irrigation     :     0.00    Transpiration     :    31.84
Bottom flux    :    -6.93    Soil evaporation  :    52.04
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    95.35    Sum               :   111.12

Period             :  01-Jan-2014 until  31-Dec-2014
Depth soil profile :  550.00 cm

            Water storage
Final   :       191.08 cm
Initial :       178.89 cm
            =============
Change           12.19 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   162.61    Interception      :     1.90
Runon          :     0.00    Runoff            :    54.27
Irrigation     :     0.00    Transpiration     :    28.12
Bottom flux    :    -1.24    Soil evaporation  :    64.88
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   161.37    Sum               :   149.18

Period             :  01-Jan-2015 until  31-Dec-2015
Depth soil profile :  550.00 cm

            Water storage
Final   :       187.67 cm
Initial :       191.08 cm
            =============
Change           -3.42 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   120.05    Interception      :     1.95
Runon          :     0.00    Runoff            :    28.72
Irrigation     :     0.00    Transpiration     :    31.65
Bottom flux    :    -5.84    Soil evaporation  :    55.30
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   114.21    Sum               :   117.62
