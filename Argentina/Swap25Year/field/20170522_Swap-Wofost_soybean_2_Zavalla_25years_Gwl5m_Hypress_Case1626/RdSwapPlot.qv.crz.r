#R function to visualise binary output (bun-files)
# commandArgs(base) 
# 06-sep-2016
#wd <- commandArgs(base)
# setwd <- "d:\\Kroes\\swap3\\PostProcessing\\RdSwapBun\\example"
#   setwd("d:\\Kroes\\SIGMA\\RegionalSwapWofostArgentina\\Simulate\\work")
#cat("working dir = "", wd)
#setwd(wd)

# -- read capillary rise at bottom root zone (output Swap in cm/d)
fname="result.crz"
tabel <- read.csv(file=fname, skip = 6, header = TRUE)
# convert the dates into a format recognised by R as a date, this is an object of class POSIXlt: 
tabel$Date  <- strptime(tabel$date, format = "%d-%b-%Y")
tabel$date  <- as.Date(tabel$date, format = "%d-%b-%Y")
tabel$year  <- tabel$Date$year+1900
tabel$month <- tabel$Date$mon+1
yrs      <- unique(tabel$year)
nrofyrs  <- length(yrs)
# qv in mm/d
mdate     <- as.Date(paste(tabel$year[1],"-",tabel$month[1],"-01", sep=""))
dum1      <- rep(mdate,(nrofyrs*12))
dum2      <- rep(NA,(nrofyrs*12))
qvCapRise_mm <- rep(NA,(nrofyrs*12))
stats <- data.frame(year=dum2,month=dum2,mdate=dum1,qvCapRise_mm=qvCapRise_mm,qvLeakage_mm=qvCapRise_mm)
dum3      <- rep(NA,(nrofyrs))
statsyr <- data.frame(year=dum3,qvCapRise_mm=dum3,qvLeakage_mm=dum3)
irec <- 0
for (iyr in 1:nrofyrs) {
	# iyr <- 1
	yr <- yrs[iyr]
	statsyr$year[iyr]  <- yr
    # each year: cap rise 
	qvc <- subset(tabel,year==yr)
	qvc$Date <- as.Date(qvc$Date)
	nrofdays <- nrow(qvc)
	for (iday in 1:nrofdays) {
		if(iday==1) {
			statsyr$qvCapRise_mm[iyr] = 0
			statsyr$qvLeakage_mm[iyr] = 0
		}
		if(qvc[iday,5]>0.0) { statsyr$qvCapRise_mm[iyr] = statsyr$qvCapRise_mm[iyr] + 10*qvc[iday,5] }
		if(qvc[iday,5]<0.0) { statsyr$qvLeakage_mm[iyr] = statsyr$qvLeakage_mm[iyr] - 10*qvc[iday,5] }
	}
    # each month: cap rise (not ready for parts of year)
	for (imonth in 1:12) {
		# imonth <- 1
		irec <- irec + 1
		stats$year[irec]  <- yr
		stats$month[irec] <- imonth
		mdate <- as.Date(paste(yr,"-",imonth,"-15", sep=""))
		stats$mdate[irec] <- mdate
		tab  <- subset(qvc,month==imonth)
        if(nrow(tab)>0) {
			nrofdays <- nrow(tab)
			for (iday in 1:nrofdays) {
				if(iday==1) {
					stats$qvCapRise_mm[irec] = 0
					stats$qvLeakage_mm[irec] = 0
				}
				if(tab[iday,5]>0.0) { stats$qvCapRise_mm[irec] = stats$qvCapRise_mm[irec] + 10*tab[iday,5] }
				if(tab[iday,5]<0.0) { stats$qvLeakage_mm[irec] = stats$qvLeakage_mm[irec] - 10*tab[iday,5] }
			}
		}
	}
}


# open and plot to file
# every day
png(file = "RdSwapBun.caprisedaily.png", width = 1024, height = 768, pointsize = 12, bg = "white")
main <- paste("Vertical flux at bottom rootzone , daily values; positive value=upward capillary flux")
plot(x = tabel$Date, y= 10*tabel[,5], type = "b", xlab = 'Date', 
     ylab = "qv (mm/d)", col = 'blue', main = main)
lines(x = tabel$Date, y= rep(0,(nrow(tabel))), type = "l", col = 'black')
dev.off()

png(file = "RdSwapBun.caprise4.png", width = 1024, height = 768, pointsize = 12, bg = "white")
par(mfrow = c(2, 2),mar = c(bottom = 2, left = 4, top = 2, right = 2))
# every day
main <- paste("Vertical flux at bottom rootzone, daily values, positive value=upward capillary flux")
plot(x = tabel$Date, y= 10*tabel[,5], type = "b", xlab = 'Date', 
     ylab = "qv (mm/d)", col = 'blue', main = main)
lines(x = tabel$Date, y= rep(0,(nrow(tabel))), type = "b", col = 'black')
# every months
main <- paste("Vertical flux at bottom rootzone , montly values")
ylim <- c(min(na.omit(-1*stats$qvLeakage_mm)), max(na.omit(stats$qvCapRise_mm)))
plot(x = stats$mdate, y= stats$qvCapRise_mm, type = "b", xlab = 'Date', 
		ylab = "qv_upward (mm/month)", col = 'blue', main = main, ylim=ylim)
lines(x = stats$mdate, y= (-1*stats$qvLeakage_mm), type = "b", col = 'red')
lines(x = stats$mdate, y= rep(0,(nrofyrs*12)), type = "l", col = 'black')
# every year
main <- paste("Vertical flux at bottom rootzone , yearly values")
ylim <- c((1.2*min(-1*statsyr$qvLeakage_mm)), (1.2*max(statsyr$qvCapRise_mm)))
plot(x = statsyr$year, y= statsyr$qvCapRise_mm, type = "h", xlab = 'Date', lwd=10,
     ylab = "qv_upward (mm/year)", col = 'blue', main = main, ylim=ylim)
lines(x = statsyr$year, y= (-1*statsyr$qvLeakage_mm), type = "h", col = 'red', lwd=10)
lines(x = statsyr$year, y= rep(0,(nrofyrs)), type = "l", col = 'black')
# every year as barplot
main <- paste("Vertical flux at bottom rootzone")
ylim <- c(0, (1.2*max(statsyr$qvCapRise_mm)))
#ylim <- c((1.2*min(statsyr$qvCapRise,statsyr$qvLeakage)), (1.2*max(statsyr$qvCapRise,statsyr$qvLeakage)))
barplot(statsyr$qvCapRise_mm, names.arg=statsyr$year, main = main, ylim = ylim, 
	 ylab = "qv_upward (mm/year)", col="blue")
#barplot(statsyr$qvLeakage, add=TRUE)
# close plotfile
dev.off()

# write output to files
zz <- file("qvRZ_year.csv", "w")
	cat("Case:",getwd(),file=zz,sep="\n")
	cat("Vertical flux (mm/yr) at bottom rootzone, positive value=upward capillary  flux",file=zz,sep="\n")
	cat("Flux of comartment nr :",names(tabel)[3],file=zz,sep="\n")
	cat("",file=zz,sep="\n")
	write.table(statsyr,file=zz,sep=",",row.names = FALSE)
	close(zz)
zz <- file("qvRZ_month.csv", "w")
	cat("Case:",getwd(),file=zz,sep="\n")
	cat("Vertical flux (mm/month) at bottom rootzone, positive value=upward capillary flux",file=zz,sep="\n")
	cat("Flux of comartment nr :",names(tabel)[3],file=zz,sep="\n")
	cat("",file=zz,sep="\n")
	write.table(stats,file=zz,sep=",",row.names = FALSE)
close(zz)

