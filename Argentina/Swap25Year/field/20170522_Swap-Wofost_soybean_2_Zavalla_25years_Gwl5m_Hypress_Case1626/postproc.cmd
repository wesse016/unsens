copy   swap_swap.log  result.run.%1.swap.log
copy   swap.ok     result.run.%1.swap.ok
copy   result.crp  result.run.%1.swap.crp
copy   result.crz  result.run.%1.swap.crz
copy   result.str  result.run.%1.swap.str
copy   result.inc  result.run.%1.swap.inc
copy   result.om1  result.run.%1.swap.om1
copy   result.om2  result.run.%1.swap.om2
copy   result.nba  result.run.%1.swap.nba
copy   Parameters_sophy.csv  result.run.%1.Parameters.sph  
rem   ----- waterbalance
copy   result.bun  result.1.bun
..\..\bin\RdSwapResultv12.exe  .\RdSwapResultv12.log  .\RdSwapResultv12.ini  .\plotnrs.csv.txt   .\   .\  
copy   RdSwapResultGg.csv  result.run.%1.gg.csv
copy   RdSwapResultYr.csv  result.run.%1.watbal.csv
rem
zip   result.run.%1.zip  result.run.%1.*
copy  result.run.%1.zip  ..\results\result.run.%1.zip 
rem pause
