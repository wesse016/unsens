* Project:       sigma
* File content:  overview of actual water and solute balance components
* File name:     Result.bal
* Model version: Swap 3.2.89
* Generated at:  24-May-2017 10:29:43

Period             :  01-Jan-1989 until  31-Dec-1989
Depth soil profile :  550.00 cm

            Water storage
Final   :       184.72 cm
Initial :       187.82 cm
            =============
Change           -3.10 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    75.23    Interception      :     0.04
Runon          :     0.00    Runoff            :    17.92
Irrigation     :     0.00    Transpiration     :     3.37
Bottom flux    :     0.00    Soil evaporation  :    56.99
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    75.23    Sum               :    78.33

Period             :  01-Jan-1990 until  31-Dec-1990
Depth soil profile :  550.00 cm

            Water storage
Final   :       184.85 cm
Initial :       184.72 cm
            =============
Change            0.13 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   102.32    Interception      :     0.38
Runon          :     0.00    Runoff            :    37.39
Irrigation     :     0.00    Transpiration     :     9.73
Bottom flux    :     0.00    Soil evaporation  :    54.70
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   102.32    Sum               :   102.19

Period             :  01-Jan-1991 until  31-Dec-1991
Depth soil profile :  550.00 cm

            Water storage
Final   :       187.88 cm
Initial :       184.85 cm
            =============
Change            3.03 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    93.07    Interception      :     0.71
Runon          :     0.00    Runoff            :    15.85
Irrigation     :     0.00    Transpiration     :    16.16
Bottom flux    :     0.01    Soil evaporation  :    57.33
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    93.08    Sum               :    90.05

Period             :  01-Jan-1992 until  31-Dec-1992
Depth soil profile :  550.00 cm

            Water storage
Final   :       185.11 cm
Initial :       187.88 cm
            =============
Change           -2.77 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   102.80    Interception      :     0.60
Runon          :     0.00    Runoff            :    23.90
Irrigation     :     0.00    Transpiration     :    17.02
Bottom flux    :     0.07    Soil evaporation  :    64.12
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   102.87    Sum               :   105.64

Period             :  01-Jan-1993 until  31-Dec-1993
Depth soil profile :  550.00 cm

            Water storage
Final   :       185.48 cm
Initial :       185.11 cm
            =============
Change            0.37 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   107.85    Interception      :     2.18
Runon          :     0.00    Runoff            :    25.08
Irrigation     :     0.00    Transpiration     :    18.89
Bottom flux    :     0.15    Soil evaporation  :    61.47
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   108.00    Sum               :   107.63

Period             :  01-Jan-1994 until  31-Dec-1994
Depth soil profile :  550.00 cm

            Water storage
Final   :       178.39 cm
Initial :       185.48 cm
            =============
Change           -7.08 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    86.31    Interception      :     1.14
Runon          :     0.00    Runoff            :    18.02
Irrigation     :     0.00    Transpiration     :    19.37
Bottom flux    :     0.22    Soil evaporation  :    55.09
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    86.53    Sum               :    93.62

Period             :  01-Jan-1995 until  31-Dec-1995
Depth soil profile :  550.00 cm

            Water storage
Final   :       176.92 cm
Initial :       178.39 cm
            =============
Change           -1.48 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    88.90    Interception      :     0.91
Runon          :     0.00    Runoff            :    24.87
Irrigation     :     0.00    Transpiration     :    18.38
Bottom flux    :     0.28    Soil evaporation  :    46.50
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    89.18    Sum               :    90.65

Period             :  01-Jan-1996 until  31-Dec-1996
Depth soil profile :  550.00 cm

            Water storage
Final   :       177.04 cm
Initial :       176.92 cm
            =============
Change            0.12 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    82.41    Interception      :     0.20
Runon          :     0.00    Runoff            :    19.08
Irrigation     :     0.00    Transpiration     :     6.15
Bottom flux    :     0.31    Soil evaporation  :    57.16
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    82.72    Sum               :    82.60

Period             :  01-Jan-1997 until  31-Dec-1997
Depth soil profile :  550.00 cm

            Water storage
Final   :       180.93 cm
Initial :       177.04 cm
            =============
Change            3.89 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    84.57    Interception      :     0.28
Runon          :     0.00    Runoff            :     8.92
Irrigation     :     0.00    Transpiration     :     6.27
Bottom flux    :     0.34    Soil evaporation  :    65.56
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    84.91    Sum               :    81.02

Period             :  01-Jan-1998 until  31-Dec-1998
Depth soil profile :  550.00 cm

            Water storage
Final   :       177.02 cm
Initial :       180.93 cm
            =============
Change           -3.91 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   101.70    Interception      :     1.63
Runon          :     0.00    Runoff            :    25.37
Irrigation     :     0.00    Transpiration     :    23.54
Bottom flux    :     0.36    Soil evaporation  :    55.43
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   102.06    Sum               :   105.98

Period             :  01-Jan-1999 until  31-Dec-1999
Depth soil profile :  550.00 cm

            Water storage
Final   :       176.55 cm
Initial :       177.02 cm
            =============
Change           -0.46 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    91.29    Interception      :     0.64
Runon          :     0.00    Runoff            :    26.43
Irrigation     :     0.00    Transpiration     :    13.44
Bottom flux    :     0.38    Soil evaporation  :    51.62
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    91.67    Sum               :    92.14

Period             :  01-Jan-2000 until  31-Dec-2000
Depth soil profile :  550.00 cm

            Water storage
Final   :       183.36 cm
Initial :       176.55 cm
            =============
Change            6.81 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   130.10    Interception      :     0.24
Runon          :     0.00    Runoff            :    42.94
Irrigation     :     0.00    Transpiration     :     7.80
Bottom flux    :     0.40    Soil evaporation  :    72.71
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   130.50    Sum               :   123.69

Period             :  01-Jan-2001 until  31-Dec-2001
Depth soil profile :  550.00 cm

            Water storage
Final   :       182.96 cm
Initial :       183.36 cm
            =============
Change           -0.40 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   130.82    Interception      :     1.51
Runon          :     0.00    Runoff            :    42.00
Irrigation     :     0.00    Transpiration     :    25.15
Bottom flux    :     0.41    Soil evaporation  :    62.97
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   131.23    Sum               :   131.63

Period             :  01-Jan-2002 until  31-Dec-2002
Depth soil profile :  550.00 cm

            Water storage
Final   :       185.41 cm
Initial :       182.96 cm
            =============
Change            2.45 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   127.74    Interception      :     0.58
Runon          :     0.00    Runoff            :    36.35
Irrigation     :     0.00    Transpiration     :    10.96
Bottom flux    :     0.41    Soil evaporation  :    77.82
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   128.15    Sum               :   125.71

Period             :  01-Jan-2003 until  31-Dec-2003
Depth soil profile :  550.00 cm

            Water storage
Final   :       180.92 cm
Initial :       185.41 cm
            =============
Change           -4.49 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   107.41    Interception      :     1.45
Runon          :     0.00    Runoff            :    26.57
Irrigation     :     0.00    Transpiration     :    24.12
Bottom flux    :     0.41    Soil evaporation  :    60.18
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   107.82    Sum               :   112.31

Period             :  01-Jan-2004 until  31-Dec-2004
Depth soil profile :  550.00 cm

            Water storage
Final   :       180.10 cm
Initial :       180.92 cm
            =============
Change           -0.81 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    93.80    Interception      :     0.41
Runon          :     0.00    Runoff            :    13.64
Irrigation     :     0.00    Transpiration     :    11.83
Bottom flux    :     0.40    Soil evaporation  :    69.13
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    94.20    Sum               :    95.01

Period             :  01-Jan-2005 until  31-Dec-2005
Depth soil profile :  550.00 cm

            Water storage
Final   :       176.79 cm
Initial :       180.10 cm
            =============
Change           -3.32 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    95.54    Interception      :     1.49
Runon          :     0.00    Runoff            :    23.17
Irrigation     :     0.00    Transpiration     :    24.62
Bottom flux    :     0.39    Soil evaporation  :    49.98
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    95.93    Sum               :    99.25

Period             :  01-Jan-2006 until  31-Dec-2006
Depth soil profile :  550.00 cm

            Water storage
Final   :       182.15 cm
Initial :       176.79 cm
            =============
Change            5.37 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   111.53    Interception      :     0.62
Runon          :     0.00    Runoff            :    21.37
Irrigation     :     0.00    Transpiration     :    12.21
Bottom flux    :     0.39    Soil evaporation  :    72.35
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   111.92    Sum               :   106.56

Period             :  01-Jan-2007 until  31-Dec-2007
Depth soil profile :  550.00 cm

            Water storage
Final   :       179.54 cm
Initial :       182.15 cm
            =============
Change           -2.61 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   114.84    Interception      :     1.14
Runon          :     0.00    Runoff            :    36.81
Irrigation     :     0.00    Transpiration     :    19.43
Bottom flux    :     0.40    Soil evaporation  :    60.47
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   115.24    Sum               :   117.85

Period             :  01-Jan-2008 until  31-Dec-2008
Depth soil profile :  550.00 cm

            Water storage
Final   :       175.78 cm
Initial :       179.54 cm
            =============
Change           -3.76 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    67.48    Interception      :     1.05
Runon          :     0.00    Runoff            :     9.49
Irrigation     :     0.00    Transpiration     :    20.58
Bottom flux    :     0.41    Soil evaporation  :    40.53
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    67.89    Sum               :    71.65

Period             :  01-Jan-2009 until  31-Dec-2009
Depth soil profile :  550.00 cm

            Water storage
Final   :       183.02 cm
Initial :       175.78 cm
            =============
Change            7.24 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   118.65    Interception      :     0.17
Runon          :     0.00    Runoff            :    28.41
Irrigation     :     0.00    Transpiration     :     5.32
Bottom flux    :     0.41    Soil evaporation  :    77.93
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   119.06    Sum               :   111.83

Period             :  01-Jan-2010 until  31-Dec-2010
Depth soil profile :  550.00 cm

            Water storage
Final   :       175.91 cm
Initial :       183.02 cm
            =============
Change           -7.10 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    86.62    Interception      :     1.06
Runon          :     0.00    Runoff            :    19.78
Irrigation     :     0.00    Transpiration     :    19.06
Bottom flux    :     0.42    Soil evaporation  :    54.25
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    87.04    Sum               :    94.14

Period             :  01-Jan-2011 until  31-Dec-2011
Depth soil profile :  550.00 cm

            Water storage
Final   :       176.94 cm
Initial :       175.91 cm
            =============
Change            1.02 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    95.60    Interception      :     0.05
Runon          :     0.00    Runoff            :    23.41
Irrigation     :     0.00    Transpiration     :     4.85
Bottom flux    :     0.42    Soil evaporation  :    66.69
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    96.02    Sum               :    95.00

Period             :  01-Jan-2012 until  31-Dec-2012
Depth soil profile :  550.00 cm

            Water storage
Final   :       182.86 cm
Initial :       176.94 cm
            =============
Change            5.93 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   137.39    Interception      :     0.23
Runon          :     0.00    Runoff            :    41.98
Irrigation     :     0.00    Transpiration     :     7.08
Bottom flux    :     0.42    Soil evaporation  :    82.60
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   137.81    Sum               :   131.89

Period             :  01-Jan-2013 until  31-Dec-2013
Depth soil profile :  550.00 cm

            Water storage
Final   :       178.19 cm
Initial :       182.86 cm
            =============
Change           -4.68 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    88.62    Interception      :     0.43
Runon          :     0.00    Runoff            :    20.41
Irrigation     :     0.00    Transpiration     :    16.00
Bottom flux    :     0.42    Soil evaporation  :    56.88
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :    89.04    Sum               :    93.72

Period             :  01-Jan-2014 until  31-Dec-2014
Depth soil profile :  550.00 cm

            Water storage
Final   :       179.81 cm
Initial :       178.19 cm
            =============
Change            1.62 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   108.30    Interception      :     1.20
Runon          :     0.00    Runoff            :    24.13
Irrigation     :     0.00    Transpiration     :    18.58
Bottom flux    :     0.42    Soil evaporation  :    63.19
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   108.72    Sum               :   107.10

Period             :  01-Jan-2015 until  31-Dec-2015
Depth soil profile :  550.00 cm

            Water storage
Final   :       180.51 cm
Initial :       179.81 cm
            =============
Change            0.70 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   110.92    Interception      :     2.05
Runon          :     0.00    Runoff            :    23.41
Irrigation     :     0.00    Transpiration     :    21.24
Bottom flux    :     0.42    Soil evaporation  :    63.94
                             Crack flux        :     0.00
                             Drainage level 1  :     0.00
=========================    ============================
Sum            :   111.34    Sum               :   110.64
