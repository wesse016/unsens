**********************************************************************************
* Filename: SIGMA.swp                                                   
* Contents: SWAP  - Main input data                                                                     
**********************************************************************************
* Comment area:
*
* Case: Argentina, soybean
*
*       includes input-switch (flCropNut) for nitrogen modelling
**********************************************************************************

*   The main input file .swp contains the following sections:
*           - General section
*           - Meteorology section
*           - Crop section
*           - Soil water section
*           - Lateral drainage section
*           - Bottom boundary section
*           - Heat flow section
*           - Solute transport section


*** GENERAL SECTION ***

**********************************************************************************
* Part 1: Environment

  PROJECT   = 'sigma'             ! Project description, [A80]
  PATHWORK  = ' '                  ! Path to work directory, [A80]
  PATHATM   = 'Weather\'      ! Path to directory with weather files, [A80]
  PATHCROP  = 'Crops\'        ! Path to directory with crop files, [A80]
  PATHDRAIN = 'Drainage\'     ! Path to directory with drainage files, [A80]
  SWSCRE    = 0          ! Switch, display progression of simulation run:
                         !   SWSCRE = 0:  no display to screen
                         !   SWSCRE = 1:  display waterbalance to screen
                         !   SWSCRE = 2:  display daynumber to screen
  SWERROR   = 0  ! Switch for printing errors to screen [Y=1, N=0]
**********************************************************************************


**********************************************************************************
* Part 2: Simulation period
*
 TSTART  = 01-jan-1989 ! Start date of simulation run, give day-month-year, [dd-mmm-yyyy]
 TEND    = 31-dec-2015 ! End   date of simulation run, give day-month-year, [dd-mmm-yyyy]
**********************************************************************************


**********************************************************************************
* Part 3: Output dates 

* Output times for water and solute balances
  SWYRVAR = 0         ! Switch, output at fixed or variable dates:
                      ! SWYRVAR = 0: each year output of balances at the same date
                      ! SWYRVAR = 1: output of balances at different dates

* If SWYRVAR = 0 specify fixed date:
  DATEFIX = 31 12     ! Specify day and month for output of yearly balances, [dd mm]

* If SWYRVAR = 1 specify all output dates [dd-mmm-yyyy], maximum MAOUT dates:
  OUTDAT =
  31-dec-1981
  31-dec-1982
* End of table

* Dates for intermediate output of state variables and fluxes
  SWMONTH = 0         ! Switch, output each month, [Y=1, N=0]
  PERIOD = 1          ! Fixed output interval, ignore = 0, [0..366, I]
  SWRES  = 0          ! Switch, reset output interval counter each year, [Y=1, N=0]
  SWODAT = 0          ! Switch, extra output dates are given in table, [Y=1, N=0]
  NPRINTDAY   = 1
  SWMETDETAIL = 0
  SWETSINE    = 0

* If SWODAT = 1, specify all intermediate output dates [dd-mmm-yyyy], 
* maximum MAOUT dates:

  OUTDATINT =
31-Jan-1980
29-Feb-1980
31-Oct-1982
30-Nov-1982
31-Dec-1982
* End of table
**********************************************************************************
                         

**********************************************************************************
* Part 4: Output files

  OUTFIL   = 'Result' ! Generic file name of output files, [A16]
  SWHEADER = 0        ! Print header of each balance period, [Y=1, N=0]
*
  SWVAP  = 0        ! Switch, output profiles of moisture, solute and temperature, [Y=1, N=0] 
  SWATE  = 0        ! Switch, output file with soil temperature profiles, [Y=1, N=0]
  SWBLC  = 1        ! Switch, output file with detailed yearly water balance, [Y=1, N=0]

* Required only when SWMACRO= 1 or 2 (see Soil Water section, Part 10: macropore flow)
  SWBMA  = 0        ! Switch, output file with detailed yearly water balance Macropores, [Y=1, N=0]

* Required only when SWDRA=2 (see lateral section): input of SWDRF and SWSWB
  SWDRF  = 0        ! Switch, output drainage fluxes, only for extended drainage, [Y=1, N=0] 
  SWSWB  = 0        ! Switch, output surface water reservoir, only for extended drainage, [Y=1, N=0]


** For water quality models or other specific use (SWAFO to DZNEW) 

* Optional output files 
  SWAFO  = 0        ! Switch, output file with formatted hydrological data
                    ! SWAFO = 0: no output
                    ! SWAFO = 1: output to a file named *.AFO
                    ! SWAFO = 2: output to a file named *.BFO

  SWAUN  = 2        ! Switch, output file with unformatted hydrological data
                    ! SWAUN = 0: no output
                    ! SWAUN = 1: output to a file named *.AUN
                    ! SWAUN = 2: output to a file named *.BUN

* Critical Deviation in water balance per OUTPUT timestep (= PERIOD)
* (when exceeded: simulation continues, but file with errors is created [file-extension *.DWB.CSV])
  CRITDEVMASBAL = 0.00001  ! Critical Deviation in water balance per OUTPUT timestep [0.0..1.0 cm, R]

  CRITDEVMASBALABS = 0.099  ! only for swap303

* if SWAFO = 1 or 2,   or if SWAUN = 1 or 2 then specify SWDISCRVERT and  CritDevMasBalAbs
  SWDISCRVERT = 0   ! Switch to convert vertical discretization   [Y=1, N=0]
                    !   only when SWAUN=1 or SWAFO=1 the generated output 
                    !   files (*.afo,*.bfo,*.aun,*.bun) are influenced
                    ! SWDISCRVERT = 0: no conversion
                    ! SWDISCRVERT = 1: convert vertical discretization, 
                    !                  numnodNew and dzNew are required
*
* Only If SWDISCRVERT = 1 then numnodNew and dzNew are required
*  NUMNODNEW = 6    ! New number of nodes [1...macp,I,-]
*                   ! (new compartments must be the sum of old compartments. See also:
*                   !  SoilWaterSection, Part4: Vertical discretization of soil profile)
*  DZNEW = 10.0 10.0 10.0 20.0 30.0 50.0 ! thickness of compartments [1.0d-6...5.0d2, cm, R]
*
**********************************************************************************


*** METEOROLOGY SECTION ***

**********************************************************************************
* General data

  METFIL = '622556'     ! File name of meteorological data without extension .YYY, [A16]
                        ! Extension equals last 3 digits of year number, e.g. 2003 has extension .003
* Use of reference evapotranspiration data from meteorological file instead of basic data
  SWETR  =  0           ! Switch, use reference ET values of meteo file [Y=1, N=0]

* If SWETR = 0, specify:
*  LAT    =   -34.2      ! Latitude of meteo station, [-60..60 degrees, R, North = +]
* zavalla grid nr 2 :
  LAT    =   -33.02      ! Latitude of meteo station, [-60..60 degrees, R, North = +]
  ALT    =    30.0      ! Altitude of meteo station, [-400..3000 m, R]
  ALTW   =    10.0      ! Altitude of wind speed measurement (10 m is default) [0..99 m, R]

*
  SWRAIN =  0           ! Switch for use of actual rainfall intensity:
                        ! SWRAIN = 0: Use daily rainfall amounts
                        ! SWRAIN = 1: Use daily rainfall amounts + mean intensity
                        ! SWRAIN = 2: Use daily rainfall amounts + duration

* If SWRAIN = 1, then specify mean rainfall intensity RAINFLUX [0.d0..1000.d0 cm/d, R]
* as function of time TIME [0..366 d, R], maximum 30 records

   TIME    RAINFLUX
    1.0         2.0
  360.0         2.0
* End of table
**********************************************************************************


*** CROP SECTION ***

**********************************************************************************
* Part 1: Crop rotation scheme during simulation period

* Specify information for each crop (maximum MACROP):
* INITCRP   = type of initialisation of crop growth: emergence (default) = 1, sowing = 2 [-]
* CROPSTART = date of crop sowing or emergence, [dd-mmm-yyyy] 
* CROPEND = date of crop harvest, [dd-mmm-yyyy]
* CROPNAME = crop name, [A16]
* CROPFIL = name of file with crop input parameters without extension .CRP, [A16]
* CROPTYPE = type of crop model: simple = 1, detailed general = 2, detailed grass = 3

  INITCRP        CROPSTART      CROPEND       CROPNAME   CROPFIL     CROPTYPE
     1      14-Nov-1989    05-May-1990   'soybean'    'SoyD'      2
     1      14-Nov-1990    05-May-1991   'soybean'    'SoyD'      2
     1      14-Nov-1991    05-May-1992   'soybean'    'SoyD'      2
     1      14-Nov-1992    05-May-1993   'soybean'    'SoyD'      2
     1      14-Nov-1993    05-May-1994   'soybean'    'SoyD'      2
     1      14-Nov-1994    05-May-1995   'soybean'    'SoyD'      2
     1      14-Nov-1995    05-May-1996   'soybean'    'SoyD'      2
     1      14-Nov-1996    05-May-1997   'soybean'    'SoyD'      2
     1      14-Nov-1997    05-May-1998   'soybean'    'SoyD'      2
     1      14-Nov-1998    05-May-1999   'soybean'    'SoyD'      2
     1      14-Nov-1999    05-May-2000   'soybean'    'SoyD'      2
     1      14-Nov-2000    05-May-2001   'soybean'    'SoyD'      2
     1      14-Nov-2001    05-May-2002   'soybean'    'SoyD'      2
     1      14-Nov-2002    05-May-2003   'soybean'    'SoyD'      2
     1      14-Nov-2003    05-May-2004   'soybean'    'SoyD'      2
     1      14-Nov-2004    05-May-2005   'soybean'    'SoyD'      2
     1      14-Nov-2005    05-May-2006   'soybean'    'SoyD'      2
     1      14-Nov-2006    05-May-2007   'soybean'    'SoyD'      2
     1      14-Nov-2007    05-May-2008   'soybean'    'SoyD'      2
     1      14-Nov-2008    05-May-2009   'soybean'    'SoyD'      2
     1      14-Nov-2009    05-May-2010   'soybean'    'SoyD'      2
     1      14-Nov-2010    05-May-2011   'soybean'    'SoyD'      2
     1      14-Nov-2011    05-May-2012   'soybean'    'SoyD'      2
     1      14-Nov-2012    05-May-2013   'soybean'    'SoyD'      2
     1      14-Nov-2013    05-May-2014   'soybean'    'SoyD'      2
     1      14-Nov-2014    05-May-2015   'soybean'    'SoyD'      2
**********************************************************************************
* flag for nitrogen in crop and soil ***

 flCropNut = .TRUE.
* flCropNut = .FALSE.


**********************************************************************************
* Part 2: Fixed irrigation applications

  SWIRFIX = 0    ! Switch for fixed irrigation applications
                 ! SWIRFIX = 0: no irrigation applications are prescribed
                 ! SWIRFIX = 1: irrigation applications are prescribed

* If SWIRFIX = 1:

  SWIRGFIL  = 0  ! Switch for file with fixed irrigation applications:
                 ! SWIRGFIL = 0: data are specified in the .swp file
                 ! SWIRGFIL = 1: data are specified in a separate file

* If SWIRGFIL  = 0 specify information for each fixed irrigation event (max. MAIRG):
* IRDATE   = date of irrigation, [dd-mmm-yyyy]
* IRDEPTH  = amount of water, [0.0..100.0 cm, R]
* IRCONC   = concentration of irrigation water, [0.0..1000.0 mg/cm3, R]
* IRTYPE   = type of irrigation: sprinkling = 0, surface = 1

      IRDATE   IRDEPTH     IRCONC   IRTYPE
 05-jan-1980       0.5     1000.0        1
* --- end of table

* If SWIRGFIL  = 1 specify name of file with data of fixed irrigation applications:
  IRGFIL = 'testirri'      ! File name without extension .IRG [A16]
**********************************************************************************


*** SOIL WATER SECTION ***

**********************************************************************************
* Part 1: Initial moisture condition

 SWINCO = 2 ! Switch, type of initial moisture condition:
            ! 1 = pressure head as function of depth is input
            ! 2 = pressure head of each compartment is in hydrostatic equilibrium 
            !     with initial groundwater level
            ! 3 = read final pressure heads from previous Swap simulation

* If SWINCO = 1, specify initial pressure head  H [-1.d10..1.d4 cm, R] as function of 
* soil depth ZI [-10000..0 cm, R], maximum MACP data pairs:
      ZI         H
   -0.5     -92.831
 -195.0      99.591
* End of table
     
* If SWINCO = 2, specify: 
  GWLI   = -500.0  ! Initial groundwater level, [-10000..100 cm, R]

* If SWINCO = 3, specify: 
  INIFIL = 'result.end'   ! name of final with extension .END [a200]
**********************************************************************************


**********************************************************************************
* Part 2: Ponding, Runoff and Runon
*
* Ponding
  PONDMX  = 1.0  ! In case of ponding, minimum thickness for runoff, [0..1000 cm, R]
*
* Runoff
 RSRO    =  0.5 ! drainage Resistance of Surface RunOff [0.001..1.0 d, R]
 RSROEXP =  1.0 ! exponent in relation of surface runoff [0.1....10.0, R]
*
*
* Runon
* Specify whether runon from external source (fiel) should be included
  SWRUNON = 0  ! Switch, input of runon:
             ! 0 = No input of runon 
             ! 1 = runon as input
*
* If SWRUNON = 1 specify name of file with runon input data 
*          - this file may be an output-*.inc-file (with only 1 header) of previous Swap-simulation):
*          - from this file 2 columns are read, with column-headers  'date' and 'Runoff'
*          - the column 'date' must have dates that correpond to the current simulation period (dates are compared)
  RUFIL = 'runon.inc' ! File name (with extension) with input data, must have extension (e.g..INC) [A80]
**********************************************************************************


**********************************************************************************
* Part 3: Soil evaporation
*
  SWCFBS = 0   ! Switch for use of soil factor CFBS to calculate Epot from ETref
               ! 0 = CFBS is not used
               ! 1 = CFBS is used 

* If SWCFBS = 1, specify soil factor CFBS:
  CFBS   = 1.0 ! Coefficient to derive Epot from ETref [0.1..1.5 -, R]
*
*
  SWREDU = 1   ! Switch, method for reduction of potential soil evaporation:
               ! 0 = reduction to maximum Darcy flux
               ! 1 = reduction to maximum Darcy flux and to maximum Black (1969)
               ! 2 = reduction to maximum Darcy flux and to maximum Bo/Str. (1986)    

 COFRED = 0.35 ! Soil evaporation coefficient of Black, [0..1 cm/d1/2, R],
               ! or Boesten/Stroosnijder, [0..1 cm1/2, R]

 RSIGNI =  0.5 ! Minimum rainfall to reset method of Black [0..1 cm/d, R]
**********************************************************************************
 

**********************************************************************************
* Part 4: Vertical discretization of soil profile

* Specify the following data (maximum MACP lines):
* ISOILLAY = number of soil layer, start with 1 at soil surface, [1..MAHO, I]
* ISUBLAY  = number of sub layer, start with 1 at soil surface, [1..MACP, I]
* HSUBLAY  = height of sub layer, [0.0..1000.0 cm, R]
* HCOMP    = height of compartments in this layer, [0.0..1000.0 cm, R]
* NCOMP    = number of compartments in this layer (= HSUBLAY/HCOMP), [1..MACP, I]


 ISOILLAY ISUBLAY  HSUBLAY    HCOMP    NCOMP
   1    1    20.0    1.0    20
   2    2    20.0    1.0    20
   3    3    20.0    1.0    20
   4    4    20.0    1.0    20
   5    5    20.0    1.0    20
   6    6    50.0    1.0    50
   7    7    50.0    1.0    50
   8    8    350.0    10.0    35
*
*
*
*
* --- end of table
**********************************************************************************


**********************************************************************************
* Part 5: Soil hydraulic functions
* as table or as function
 SWSOPHY = 0   ! Switch for use of tables or functions[tables=1, functions=0]
* If SWSOPHY = 1 then supply input data for tables: 
  ! FILENAMESOPHY = File names  (with extension) of input data, must have extension (e.g..INC) [A80]
  FILENAMESOPHY = 'starb17_cm.csv', 'staro17_cm.csv', 'staro17_cm.csv', 'staro17_cm.csv'

* Specify for each soil layer (maximum MAHO):
* ISOILLAY1 = number of soil layer, as defined in part 4 [1..MAHO, I]
* ORES   = Residual water content, [0..0.4 cm3/cm3, R]
* OSAT   = Saturated water content, [0..0.95 cm3/cm3, R]
* ALFA   = Shape parameter alfa of main drying curve, [0.0001..1 /cm, R]
* NPAR   = Shape parameter n, [1..4 -, R]
* KSAT   = Saturated vertical hydraulic conductivity, [1.d-5..1000 cm/d, R]
* LEXP   = Exponent in hydraulic conductivity function, [-25..25 -, R]
* ALFAW  = Alfa parameter of main wetting curve in case of hysteresis, [0.0001..1 /cm, R]
* H_ENPR = Air entry pressure head [-40.0..0.0 cm, R]

* 1. For this example data were taken from the StaringSeries using indications from the
* soil profile description, such as Eq.humedad (%) and particle size distribution:
*  first guess: similar to San Antonio; only layers 4 and 5 exchanged
*   to be improved !
  ISOILLAY1,  ORES,  OSAT,  ALFA,  NPAR,  KSAT,   LEXP,   ALFAW  H_ENPR 
1  0.0  0.407464380885715  0.001  1.14809607678371  0.274483098101885  -2.1146217380253  0.001  0.0
2  0.0  0.396892047245069  0.0063647747620893  1.07545982697416  0.762352612172074  -3.38286502500965  0.0063647747620893  0.0
3  0.0  0.397977063549221  0.0139248160288246  1.06309939799568  1.48113619312819  -4.27434336964157  0.0139248160288246  0.0
4  0.0  0.403393785762343  0.0200801427663392  1.06711687712021  2.63186433888569  -4.55987199438348  0.0200801427663392  0.0
5  0.0  0.40369188299653  0.0239883672632622  1.07535086768716  4.23889237228787  -4.68907381255294  0.0239883672632622  0.0
6  0.0  0.398702038048484  0.027523588479564  1.08417193273573  6.01854215094617  -4.75187972135254  0.027523588479564  0.0
7  0.0  0.371229711511949  0.0214529115780974  1.07408240128645  4.3891737980239  -5.15924095473386  0.0214529115780974  0.0
8  0.0  0.371229711511949  0.0214529115780974  1.07408240128645  4.3891737980239  -5.15924095473386  0.0214529115780974  0.0
*
*
*
*

* --- end of table


**********************************************************************************


**********************************************************************************
* Part 6: Hysteresis of soil water retention function

* Switch for hysteresis:
  SWHYST = 0   ! Switch for hysteresis:
               ! 0 = no hysteresis                                             
               ! 1 = hysteresis, initial condition wetting                                 
               ! 2 = hysteresis, initial condition drying

* If SWHYST = 1 or 2, specify:                                      
  TAU = 0.2    ! Minimum pressure head difference to change wetting-drying, [0..1 cm, R]
**********************************************************************************


**********************************************************************************
* Part 7: Maximum rooting depth

  RDS  =  150.0000
**********************************************************************************


**********************************************************************************
* Part 8: Similar media scaling of soil hydraulic functions

  SWSCAL = 0 ! Switch for similar media scaling [Y=1, N=0]; no hysteresis is allowed
             ! in case of similar media scaling (SWHYST = 0)

* If SWSCAL = 1, specify:                                                        
  NSCALE = 3 ! Number of simulation runs, [1..MASCALE, I]

* Supply the scaling factors for each simulation run and each soil layer:

  RUN     SOIL1        SOIL2
   1       0.5          2.0
   2       1.0          1.0
   3       2.0          0.5
   4       1.0          1.0
   5       3.0          3.0
* End of table
**********************************************************************************


**********************************************************************************
* Part 9: Preferential flow due to macropores
  SWMACRO = 0     ! Switch for macropore flow, [0..2, I]:
                  ! 0 = no macropore flow
                  ! 1 = simple macropore flow
                  ! 2 = advanced macropore flow


**********************************************************************************
* Part 10: Snow and frost
*
  SWSNOW = 0   ! Switch, calculate snow accumulation and melt. [Y=1, N=0]
*                
** If SWSNOW = 1, specify:
  SNOWINCO = 22.0      ! Initial snow water equivalent, [0.0...1000.0 cm, R] 
  TEPRRAIN = 2.0       ! Temperature above which all precipitation is rain,[ 0.0...5.0 �C, R]
  TEPRSNOW = -2.0      ! Temperature below which all precipitation is snow,[-5.0...0.0 �C, R]
  SNOWCOEF = 0.3       ! Snowmelt calibration factor, [0.0...10.0 -, R]

* Frost
  SWFROST = 0  ! Switch, in case of frost: reduce soil water flow, [Y=1, N=0]

* If SWFROST = 1, then specify soil temperature to start end end flux-reduction
  tfroststa = 0.0      ! Soil temperature (�C) where reduction of water fluxes starts [-10.0,5.0, oC, R]
  tfrostend = -1.0     ! Soil temperature (�C) where reduction of water fluxes ends [-10.0,5.0, oC, R]
**********************************************************************************


**********************************************************************************
* Part 12 Numerical solution of Richards' equation
*
 DTMIN     =  1.0d-6     ! Minimum timestep, [1.d-8..0.1 d, R]
 DTMAX     =   0.2       ! Maximum timestep, [ 0.01..0.5 d, R]
 GWLCONV   =  100.0      ! Maximum dif. groundwater level between iterations, [1.d-5..1000 cm, R]
 CritDevPondDt = 1.0d-4  ! Critical Deviation in water balance of ponding layer [1.0d-5..100.0 cm, R]
 CritDevh1Cp   = 1.0d-2  ! Convergence criterium for Richards equation: relative difference in pressure heads (-)
 CritDevh2Cp   = 1.0d-1  ! Convergence criterium for Richards equation: absolute difference in pressure heads (L)
 MaxIt     = 30          ! Maximum number of iterations [5,100 -,I]
 MaxBackTr = 3           ! Maximum number of back track cycles within an iteration cycle [1,10 -,I]
*
* Switch for mean of hydraulic conductivity, [1,2,3,4, I]
*  SWkmean = 1: unweighted arithmic mean
*  SWkmean = 2: weighted arithmic mean
*  SWkmean = 3: unweighted geometric mean
*  SWkmean = 4: weighted geometric mean
SWkmean = 2  

* Switch for implicit solution with hydraulic conductivity: 0 = explicit, 1 = implicit
SWkImpl = 0   ! Switch for kmean in implicit numerical solution with, [0,1, I]

* Flag to enable an interrupt in simulation with (near) endless iterations
      flMaxIterTime = .true.        ! flag to activate controle of  MaxIterTime [.false., .true., -,L]
* maximum cpu time , maximum range of 2419200 secs equals 4 weeks (60*60*24*7*4=2419200 secs)
       MaxIterTime = 3600        ! maximum cpu time  [1, 2419200 secs,I]


**********************************************************************************


*** LATERAL DRAINAGE SECTION ***

**********************************************************************************
* Specify whether lateral drainage to surface water should be included
*
  SWDRA = 1  ! Switch, simulation of lateral drainage:
             ! 0 = No simulation of drainage                                 
             ! 1 = Simulation with basic drainage routine                       
             ! 2 = Simulation of drainage with surface water management

* If SWDRA = 1 or SWDRA = 2 specify name of file with drainage input data:
  DRFIL = 'Pampas' ! File name with drainage input data without extension .DRA, [A16]
**********************************************************************************
                                                                       

*** BOTTOM BOUNDARY SECTION ***

**********************************************************************************
* Bottom boundary condition

  SWBBCFILE  = 1    ! Switch for file with bottom boundary conditions:
                    ! SWBBCFILE = 0: data are specified in the .swp file
                    ! SWBBCFILE = 1: data are specified in a separate file

* If SWBBCFILE = 1 specify name of file with bottom boundary conditions:
  BBCFIL = 'sigma'      ! File name without extension .BBC [A16]

* If SWBBCFILE = 0, select one of the following options:
             ! 1  Prescribe groundwater level
             ! 2  Prescribe bottom flux
             ! 3  Calculate bottom flux from hydraulic head of deep aquifer
             ! 4  Calculate bottom flux as function of groundwater level
             ! 5  Prescribe soil water pressure head of bottom compartment
             ! 6  Bottom flux equals zero
             ! 7  Free drainage of soil profile
             ! 8  Free outflow at soil-air interface

 SWBOTB = 6  ! Switch for bottom boundary [1..8,-,I]

* Options 1,2,3,4,and 5 require additional data as specified below!
**********************************************************************************


**********************************************************************************
* SWBOTB = 1  Prescribe groundwater level

* specify DATE [dd-mmm-yyyy] and groundwater level [cm, -10000..1000, R] 

        DATE1    GWLEVEL         ! (max. MABBC records)
  01-jan-1981     -95.0
  31-dec-1983     -95.0
* End of table                                                     
**********************************************************************************


**********************************************************************************
* SWBOTB = 2   Prescribe bottom flux

* Specify whether a sine or a table are used to prescribe the bottom flux:
  SW2    = 2      ! Sine function = 1,  table = 2

* In case of sine function (SW2 = 1), specify:
  SINAVE =  0.1   ! Average value of bottom flux, [-10..10 cm/d, R, + = upwards]
  SINAMP =  0.05  ! Amplitude of bottom flux sine function, [-10..10 cm/d, R]
  SINMAX =  91.0  ! Time of the year with maximum bottom flux, [1..366 d, R]  

* In case of table (SW2 = 2), specify date [dd-mmm-yyyy] and bottom flux QBOT2
* [-100..100 cm/d, R, positive = upwards]:

        DATE2     QBOT2           ! (maximum MABBC records)
  01-jan-1980       0.1
  30-jun-1980       0.2
  23-dec-1980      0.15
* End of table
**********************************************************************************


**********************************************************************************
* SWBOTB = 3    Calculate bottom flux from hydraulic head in deep aquifer

* Switch to suppress addition of vertical resistance between bottom of model and 
* groundwater level 0 = default, 1 = suppress
  SWBOTB3RESVERT = 0 ! Switch to suppress additional resistance [0,1, I]

* Switch for implicit solution with bottom flux: 0 = explicit, 1 = implicit
* Do not use SWBOTB3IMPL = 1 in combination with SHAPE < 1.0
  SWBOTB3IMPL = 0   ! Switch for kmean in implicit numerical solution with, [0,1, I]

* Specify:
  SHAPE  =   0.79  ! Shape factor to derive average groundwater level, [0..1 -, R]
  HDRAIN =  -110.0 ! Mean drain base to correct for average groundwater level, [-10000..0 cm, R]
  RIMLAY =   500.0 ! Vertical resistance of aquitard, [0..10000 d, R]

* Specify whether a sine or a table are used to prescribe hydraulic head of deep aquifer:
  SW3    = 1       ! 1 = Sine function,  2 = table 

* In case of sine function (SW3  = 1), specify:
  AQAVE  =  -500.0 ! Average hydraulic head in underlaying aquifer, [-10000..1000 cm, R] 
  AQAMP  =    20.0 ! Amplitude hydraulic head sinus wave, [0..1000 cm, R]
  AQTMAX =  120.0  ! First time of the year with maximum hydraulic head, [1..366 d, R]
  AQPER  =  365.0  ! Period hydraulic head sinus wave, [1..366 d, I]

* In case of table (SW3  = 2), specify date [dd-mmm-yyyy] and average hydraulic head 
* HAQUIF in underlaying aquifer [-10000..1000 cm, R]:

        DATE3    HAQUIF           ! (maximum MABBC records)
  01-jan-1980     -95.0
  30-jun-1980    -110.0
  23-dec-1980     -70.0
* End of table

* An extra groundwater flux can be specified which is added to above specified flux
  SW4   = 1        ! 0 = no extra flux, 1 = include extra flux

* If SW4 = 1, specify date [dd-mmm-yyyy] and bottom flux QBOT4 [-100..100 cm/d, R, 
* positive = upwards]:

        DATE4     QBOT4           ! (maximum MABBC records)
  01-jan-1980       1.0
  30-jun-1980     -0.15
  23-dec-1980       1.2
* End of table
**********************************************************************************


**********************************************************************************
* SWBOTB = 4     Calculate bottom flux as function of groundwater level

* Specify whether an exponential relation or a table is used to determine qbot from groundwaterlevel:
  SWQHBOT = 2       ! 1 = exponential relation,  2 = table 
 
* In case of an exponential relation (SWQHBOT  = 1),
* Specify coefficients of relation qbot = A exp (B*abs(groundwater level))
  COFQHA =  0.1  ! Coefficient A, [-100..100 cm/d, R]
  COFQHB =  0.5  ! Coefficient B  [-1..1 /cm, R]

* In case of a table (SWQHBOT  = 2),
* Specify groundwaterlevel Htab [-10000..1000, cm, R]  and bottom flux QTAB [-100..100 cm/d, R]
* Htab is negative below the soil surface, Qtab is negative when flux is downward.
  HTAB   QTAB
  -0.1   -0.35
  -70.0  -0.05
 -125.0  -0.01
**********************************************************************************


**********************************************************************************
* SWBOTB = 5     Prescribe soil water pressure head of bottom compartment
 
* Specify DATE [dd-mmm-yyyy] and bottom compartment pressure head HBOT5 
* [-1.d10..1000 cm, R]: 

        DATE5     HBOT5           ! (maximum MABBC records)
  01-jan-1980     -95.0
  30-jun-1980    -110.0
  23-dec-1980     -70.0
* End of table
**********************************************************************************


*** HEAT FLOW SECTION ***

**********************************************************************************
* Part 1: Specify whether simulation includes heat flow

  SWHEA  = 0 ! Switch for simulation of heat transport, [Y=1, N=0]
**********************************************************************************

**********************************************************************************
* Part 2: Heat flow calculation method

  SWCALT = 2     ! Switch for method: 1 = analytical method, 2 = numerical method
**********************************************************************************

**********************************************************************************
* Analytical method

* If SWCALT = 1 specify the following heat parameters:
  TAMPLI = 10.0 ! Amplitude of annual temperature wave at soil surface, [0..50 C, R]
  TMEAN  = 15.0 ! Mean annual temperature at soil surface, [5..30 C, R]
  TIMREF = 90.0 ! Time in the year with top of sine temperature wave [1..366 d, R]
  DDAMP  = 50.0 ! Damping depth of temperature wave in soil, [0..500 cm, R]
**********************************************************************************

**********************************************************************************
* Numerical method

* If SWCALT = 2 list initial temperature TSOIL [-20..40 C, R] as function of 
* soil depth ZH [-1d5..0 cm, R]:
* When SWINCO = 3, dummy values can be present for ZH and TSOIL, because real values 
* are read from file INIFIL (see this file:  Soil Water section, Part 1)
      ZH    TSOIL           ! (maximum MACP records)
   -10.0      5.0
  -100.0      9.0
* End of table

* if SWCALT = 2 then define top boundary condition 
  SwTopbHea = 1        ! 1 = use air temperatures; 2 = read measured surface temperatures

* if SWCALT = 2 then define lower boundary condition (
  SwBotbHea = 1          ! Switch for bbc: 1 = 0.0, 2 = input of Temperature(time)
* if SwBotbHea = 2 then specify a tabel with dates and temperatures for lower boundary
  datet          tbot
  01-jan-1980    -15.0
  30-jun-1980    -20.0
  23-dec-1980    -10.0

* If SWCALT = 2 specify for each soil type the soil texture (g/g mineral parts)
* and the organic matter content (g/g dry soil):

  ISOILLAY5  PSAND    PSILT    PCLAY    ORGMAT           ! (maximum MAHO records)
   1    0.33    0.42    0.25    0.04
   2    0.29    0.38    0.33    0.02
   3    0.26    0.34    0.40    0.01
   4    0.26    0.34    0.40    0.01
   5    0.26    0.36    0.38    0.01
   6    0.28    0.36    0.36    0.00
   7    0.25    0.37    0.38    0.00
   8    0.25    0.37    0.38    0.00
*
*
*
*
* End of table
**********************************************************************************

*** SOLUTE SECTION ***

**********************************************************************************
* Part 1: Specify whether simulation includes solute transport

  SWSOLU = 0 ! Switch for simulation of solute transport, [Y=1, N=0]
**********************************************************************************


**********************************************************************************
* Part 2: Top boundary and initial condition

  CPRE = 0.0    ! Solute concentration in precipitation, [1..100 mg/cm3, R]

* List initial solute concentration CML [1..1000 mg/cm3, R] as function of soil depth ZC
* [-10000..0 cm, R], max. MACP records:
* When SWINCO=3, then dummy values must be present for ZC and CML, because real values 
* are read from file INIFIL (See this file:  SOIL WATER SECTION, part 1)
      ZC       CML
   -10.0       0.0
   -95.0       0.0
* End of table
**********************************************************************************


**********************************************************************************
* Part 3: Miscellaneous parameters as function of soil depth

* Specify for each soil layer (maximum MAHO)
* ISOILLAY6 = number of soil layer, as defined in soil water section (part 4) [1..MAHO, I]
* LDIS      = dispersion length, [0..100 cm, R]
* KF        = Freundlich adsorption coefficient, [0..100 cm3/mg, R]
* BDENS     = dry soil bulk density, [500..3000 mg/cm3, R]
* DECPOT    = potential decomposition rate, [0..10 /d, R]
*
 ISOILLAY6     LDIS          KF     BDENS  DECPOT
   1    5.0    0.0001    1400.0    0.0
   2    5.0    0.0001    1460.0    0.0
   3    5.0    0.0001    1490.0    0.0
   4    5.0    0.0001    1500.0    0.0
   5    5.0    0.0001    1520.0    0.0
   6    5.0    0.0001    1550.0    0.0
   7    5.0    0.0001    1650.0    0.0
   8    5.0    0.0001    1650.0    0.0
*
*
*
*

* --- end of Table
**********************************************************************************


**********************************************************************************
* Part 4: Diffusion constant and solute uptake by roots

  DDIF = 0.0    ! Molecular diffusion coefficient, [0..10 cm2/day, R]
  TSCF = 0.0    ! Relative uptake of solutes by roots, [0..10 -, R]
**********************************************************************************

 
**********************************************************************************
* Part 4: Adsorption 

  SWSP = 0      ! Switch, consider solute adsorption, [Y=1, N=0]

* In case of adsorption (SWSP = 1), specify:
  FREXP = 0.9   ! Freundlich exponent, [0..10 -, R]
  CREF  = 1.0   ! Reference solute concentration for adsorption, [0..1000 mg/cm3, R]
**********************************************************************************


**********************************************************************************
* Part 5: Decomposition

  SWDC = 0      ! Switch, consideration of solute decomposition, [Y=1, N=0]

* In case of solute decomposition (SWDC = 1), specify:
  GAMPAR = 0.0  ! Factor reduction decomposition due to temperature, [0..0.5 /C, R]
  RTHETA = 0.3  ! Minimum water content for potential decomposition, [0..0.4 cm3/cm3, R]
  BEXP   = 0.7  ! Exponent in reduction decomposition due to dryness, [0..2 -, R]

* List the reduction of pot. decomposition for each soil type, [0..1 -, R]:

  ISOILLAY7  FDEPTH           ! (maximum MAHO records)
       1       1.00
       2       0.65
* End of table
**********************************************************************************


**********************************************************************************
* Part 7: Solute residence in the saturated zone

  SWBR = 0       ! Switch, consider mixed reservoir of saturated zone [Y=1, N=0]

* Without mixed reservoir (SWBR = 0), specify:
  CDRAIN = 0.1   ! solute concentration in groundwater, [0..100 mg/cm3, R]

* In case of mixed reservoir (SWBR = 1), specify:
  DAQUIF = 110.0 ! Thickness saturated part of aquifer, [0..10000 cm, R]
  POROS  = 0.4   ! Porosity of aquifer, [0..0.6, R]
  KFSAT  = 0.2   ! Linear adsorption coefficient in aquifer, [0..100 cm3/mg, R]
  DECSAT = 1.0   ! Decomposition rate in aquifer, [0..10 /d, R]
  CDRAINI = 0.2  ! Initial solute concentration in groundwater, [0..100 mg/cm3, R]
**********************************************************************************

* End of the main input file .SWP!
SWCAPRISEOUTPUT = .true.
* GRID_NO =  622556
* CROP_NO =  1
* Variety_NO =  65
* Swap_CropFile =  SoyD'
* SOIL_NO association(STU) =  21328
* SOIL_NO (SMU) =  12290
* Simulation_NO =  3
