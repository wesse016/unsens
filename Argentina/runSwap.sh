#!/bin/sh
#SBATCH --partition=ESG_Low
#SBATCH --time=01:00:00
#SBATCH --nodes=5
#SBATCH --ntasks-per-node=8
##SBATCH --mem-per-cpu=5000
#SBATCH --job-name="swap IRC"
#SBATCH --array=1-36
#SBATCH --output="log/output-%A_%a.out"
#SBATCH --error="log/error-%A_%a.out"
#SBATCH --mail-user=jan.wesseling@wur.nl
#SBATCH --mail-type=ALL
#SBATCH --tmp=50M
##SBATCH --requeue
#Specifies that the job will be requeued after a node failure.
#The default is that the job will not be requeued.

echo "SLURM_JOBID="$SLURM_JOBID
echo "SLURM_JOB_NODELIST"=$SLURM_JOB_NODELIST
echo "SLURM_NNODES"=$SLURM_NNODES
echo "SLURMTMPDIR="$SLURMTMPDIR
echo "SLURM_ARRAYID="$SLURM_ARRAYID
echo "SLURM_ARRAY_JOB_ID"=$SLURM_ARRAY_JOB_ID
echo "SLURM_ARRAY_TASK_ID"=$SLURM_ARRAY_TASK_ID
echo "working directory = "$SLURM_SUBMIT_DIR

NPROCS=`srun --nodes=${SLURM_NNODES} bash -c 'hostname' |wc -l`
echo "NPROCS="$NPROCS

ulimit -s unlimited

echo "run swap computation"
sh /lustre/scratch/WUR/ESG/wesse016/run${SLURM_ARRAY_TASK_ID}/run.sh
echo "All Done!"
