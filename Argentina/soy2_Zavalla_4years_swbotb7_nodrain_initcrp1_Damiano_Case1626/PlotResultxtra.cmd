rem -- extra plots, assuming that ouput-files have a filename "result"
rem     otherwise copy or rename *.inc, *.crp, *.str 
rem     example:  copy run.145.swap.inc result.inc
rem set Rexe= "c:\Program Files\R\R-3.0.2\bin\x64\Rterm.exe" --no-restore --no-save
R --no-restore --no-save < ReadCrp.R          > ReadCrpR.log
R --no-restore --no-save < ReadInc.R          > ReadIncR.log
R --no-restore --no-save < ReadET.R           > ReadETR.log
rem R --no-restore --no-save < RdSwapResultDt.R   > RdSwapResultDt.R.log
R --no-restore --no-save < Swap_Stress.R      > Swap_StressR.log
rem R --no-restore --no-save < contourResultVap.R > contourResultVap.R.log
rem   Plot_Yact_Tact.R needs adjustment for kind of crop and nr of years!
R --no-restore --no-save < Plot_Yact_Tact.R   > Plot_Yact_Tact.R.log

rem pause

rem  exit
