* Project:       sigma
* File content:  overview of actual water and solute balance components
* File name:     Result.bal
* Model version: Swap 4.0.10c
* Generated at:  2018-03-28 20:46:14

Period             :  2011-01-01  until  2011-12-31 
Depth soil profile :  550.00 cm

            Water storage
Final   :       205.70 cm
Initial :       235.99 cm
            =============
Change          -30.30 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    86.02    Interception      :     0.03
Runon          :     0.00    Runoff            :     1.13
Irrigation     :     0.00    Transpiration     :    12.37
Bottom flux    :   -73.91    Soil evaporation  :    28.87
                             Crack flux        :     0.00
=========================    ============================
Sum            :    12.11    Sum               :    42.41

Period             :  2012-01-01  until  2012-12-31 
Depth soil profile :  550.00 cm

            Water storage
Final   :       225.89 cm
Initial :       205.70 cm
            =============
Change           20.20 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   136.04    Interception      :     0.88
Runon          :     0.00    Runoff            :     9.12
Irrigation     :     0.00    Transpiration     :    37.70
Bottom flux    :   -41.10    Soil evaporation  :    27.04
                             Crack flux        :     0.00
=========================    ============================
Sum            :    94.94    Sum               :    74.74

Period             :  2013-01-01  until  2013-12-31 
Depth soil profile :  550.00 cm

            Water storage
Final   :       193.28 cm
Initial :       225.89 cm
            =============
Change          -32.61 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    55.27    Interception      :     2.16
Runon          :     0.00    Runoff            :     0.26
Irrigation     :     0.00    Transpiration     :    38.63
Bottom flux    :   -26.54    Soil evaporation  :    20.30
                             Crack flux        :     0.00
=========================    ============================
Sum            :    28.73    Sum               :    61.34

Period             :  2014-01-01  until  2014-12-31 
Depth soil profile :  550.00 cm

            Water storage
Final   :       214.57 cm
Initial :       193.28 cm
            =============
Change           21.29 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   111.57    Interception      :     1.76
Runon          :     0.00    Runoff            :     2.79
Irrigation     :     0.00    Transpiration     :    35.05
Bottom flux    :   -24.64    Soil evaporation  :    26.04
                             Crack flux        :     0.00
=========================    ============================
Sum            :    86.93    Sum               :    65.64
