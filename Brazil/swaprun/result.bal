* Project:       sense
* File content:  overview of actual water and solute balance components
* File name:     ./result.bal
* Model version: Swap 4.0.1
* Generated at:  2018-11-20 18:17:04

Period             :  1987-01-01  until  1987-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       794.92 cm
Initial :       767.96 cm
            =============
Change           26.96 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   150.51    Interception      :     0.25
Runon          :     0.00    Runoff            :    12.07
Irrigation     :     0.00    Transpiration     :    19.61
Bottom flux    :   -55.47    Soil evaporation  :    36.14
                             Crack flux        :     0.00
=========================    ============================
Sum            :    95.04    Sum               :    68.08

Period             :  1988-01-01  until  1988-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       796.79 cm
Initial :       794.92 cm
            =============
Change            1.87 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   146.64    Interception      :     0.18
Runon          :     0.00    Runoff            :    24.77
Irrigation     :     0.00    Transpiration     :    20.09
Bottom flux    :   -64.43    Soil evaporation  :    35.31
                             Crack flux        :     0.00
=========================    ============================
Sum            :    82.21    Sum               :    80.34

Period             :  1989-01-01  until  1989-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       788.33 cm
Initial :       796.79 cm
            =============
Change           -8.46 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   144.66    Interception      :     0.56
Runon          :     0.00    Runoff            :    23.46
Irrigation     :     0.00    Transpiration     :    25.80
Bottom flux    :   -65.15    Soil evaporation  :    38.16
                             Crack flux        :     0.00
=========================    ============================
Sum            :    79.51    Sum               :    87.98

Period             :  1990-01-01  until  1990-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       785.17 cm
Initial :       788.33 cm
            =============
Change           -3.15 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   130.19    Interception      :     0.28
Runon          :     0.00    Runoff            :    12.57
Irrigation     :     0.00    Transpiration     :    19.83
Bottom flux    :   -63.56    Soil evaporation  :    37.10
                             Crack flux        :     0.00
=========================    ============================
Sum            :    66.63    Sum               :    69.79

Period             :  1991-01-01  until  1991-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       791.45 cm
Initial :       785.17 cm
            =============
Change            6.28 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   153.35    Interception      :     0.28
Runon          :     0.00    Runoff            :    35.09
Irrigation     :     0.00    Transpiration     :    18.66
Bottom flux    :   -58.76    Soil evaporation  :    34.28
                             Crack flux        :     0.00
=========================    ============================
Sum            :    94.59    Sum               :    88.31

Period             :  1992-01-01  until  1992-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       797.22 cm
Initial :       791.45 cm
            =============
Change            5.77 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   131.04    Interception      :     0.16
Runon          :     0.00    Runoff            :    10.66
Irrigation     :     0.00    Transpiration     :    15.14
Bottom flux    :   -63.72    Soil evaporation  :    35.60
                             Crack flux        :     0.00
=========================    ============================
Sum            :    67.32    Sum               :    61.56

Period             :  1993-01-01  until  1993-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       794.37 cm
Initial :       797.22 cm
            =============
Change           -2.86 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   132.36    Interception      :     0.25
Runon          :     0.00    Runoff            :     8.84
Irrigation     :     0.00    Transpiration     :    17.81
Bottom flux    :   -72.04    Soil evaporation  :    36.26
                             Crack flux        :     0.00
=========================    ============================
Sum            :    60.32    Sum               :    63.17

Period             :  1994-01-01  until  1994-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       808.52 cm
Initial :       794.37 cm
            =============
Change           14.15 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   138.17    Interception      :     0.14
Runon          :     0.00    Runoff            :     7.71
Irrigation     :     0.00    Transpiration     :    14.25
Bottom flux    :   -64.28    Soil evaporation  :    37.65
                             Crack flux        :     0.00
=========================    ============================
Sum            :    73.89    Sum               :    59.74

Period             :  1995-01-01  until  1995-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       806.03 cm
Initial :       808.52 cm
            =============
Change           -2.49 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   164.75    Interception      :     0.21
Runon          :     0.00    Runoff            :    38.33
Irrigation     :     0.00    Transpiration     :    15.25
Bottom flux    :   -74.21    Soil evaporation  :    39.25
                             Crack flux        :     0.00
=========================    ============================
Sum            :    90.54    Sum               :    93.04

Period             :  1996-01-01  until  1996-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       803.85 cm
Initial :       806.03 cm
            =============
Change           -2.18 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   159.12    Interception      :     0.31
Runon          :     0.00    Runoff            :    31.91
Irrigation     :     0.00    Transpiration     :    12.74
Bottom flux    :   -73.62    Soil evaporation  :    42.72
                             Crack flux        :     0.00
=========================    ============================
Sum            :    85.50    Sum               :    87.68

Period             :  1997-01-01  until  1997-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       803.10 cm
Initial :       803.85 cm
            =============
Change           -0.74 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   135.91    Interception      :     0.25
Runon          :     0.00    Runoff            :    17.70
Irrigation     :     0.00    Transpiration     :    13.45
Bottom flux    :   -68.70    Soil evaporation  :    36.54
                             Crack flux        :     0.00
=========================    ============================
Sum            :    67.21    Sum               :    67.95

Period             :  1998-01-01  until  1998-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       803.73 cm
Initial :       803.10 cm
            =============
Change            0.63 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   142.94    Interception      :     0.34
Runon          :     0.00    Runoff            :    17.48
Irrigation     :     0.00    Transpiration     :    15.53
Bottom flux    :   -71.59    Soil evaporation  :    37.38
                             Crack flux        :     0.00
=========================    ============================
Sum            :    71.35    Sum               :    70.72

Period             :  1999-01-01  until  1999-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       795.02 cm
Initial :       803.73 cm
            =============
Change           -8.72 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   143.99    Interception      :     0.44
Runon          :     0.00    Runoff            :    29.85
Irrigation     :     0.00    Transpiration     :    18.00
Bottom flux    :   -66.16    Soil evaporation  :    38.26
                             Crack flux        :     0.00
=========================    ============================
Sum            :    77.83    Sum               :    86.55

Period             :  2000-01-01  until  2000-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       806.66 cm
Initial :       795.02 cm
            =============
Change           11.64 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   134.21    Interception      :     0.43
Runon          :     0.00    Runoff            :     7.04
Irrigation     :     0.00    Transpiration     :    15.72
Bottom flux    :   -65.34    Soil evaporation  :    34.03
                             Crack flux        :     0.00
=========================    ============================
Sum            :    68.87    Sum               :    57.23

Period             :  2001-01-01  until  2001-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       802.06 cm
Initial :       806.66 cm
            =============
Change           -4.59 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   124.08    Interception      :     0.57
Runon          :     0.00    Runoff            :     7.60
Irrigation     :     0.00    Transpiration     :    19.99
Bottom flux    :   -61.93    Soil evaporation  :    38.59
                             Crack flux        :     0.00
=========================    ============================
Sum            :    62.15    Sum               :    66.74

Period             :  2002-01-01  until  2002-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       798.19 cm
Initial :       802.06 cm
            =============
Change           -3.87 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   145.92    Interception      :     0.28
Runon          :     0.00    Runoff            :    27.97
Irrigation     :     0.00    Transpiration     :    15.42
Bottom flux    :   -67.70    Soil evaporation  :    38.42
                             Crack flux        :     0.00
=========================    ============================
Sum            :    78.22    Sum               :    82.09

Period             :  2003-01-01  until  2003-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       796.31 cm
Initial :       798.19 cm
            =============
Change           -1.88 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   110.44    Interception      :     0.39
Runon          :     0.00    Runoff            :     9.63
Irrigation     :     0.00    Transpiration     :    15.19
Bottom flux    :   -55.71    Soil evaporation  :    31.40
                             Crack flux        :     0.00
=========================    ============================
Sum            :    54.73    Sum               :    56.61

Period             :  2004-01-01  until  2004-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       792.71 cm
Initial :       796.31 cm
            =============
Change           -3.60 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   116.55    Interception      :     0.23
Runon          :     0.00    Runoff            :     8.72
Irrigation     :     0.00    Transpiration     :    15.08
Bottom flux    :   -63.51    Soil evaporation  :    32.61
                             Crack flux        :     0.00
=========================    ============================
Sum            :    53.04    Sum               :    56.65

Period             :  2005-01-01  until  2005-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       795.34 cm
Initial :       792.71 cm
            =============
Change            2.63 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   109.15    Interception      :     0.27
Runon          :     0.00    Runoff            :     7.20
Irrigation     :     0.00    Transpiration     :    14.77
Bottom flux    :   -53.92    Soil evaporation  :    30.36
                             Crack flux        :     0.00
=========================    ============================
Sum            :    55.23    Sum               :    52.60

Period             :  2006-01-01  until  2006-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       807.94 cm
Initial :       795.34 cm
            =============
Change           12.60 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   126.17    Interception      :     0.51
Runon          :     0.00    Runoff            :     6.96
Irrigation     :     0.00    Transpiration     :    15.00
Bottom flux    :   -56.06    Soil evaporation  :    35.04
                             Crack flux        :     0.00
=========================    ============================
Sum            :    70.11    Sum               :    57.51

Period             :  2007-01-01  until  2007-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       797.56 cm
Initial :       807.94 cm
            =============
Change          -10.38 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   127.99    Interception      :     0.20
Runon          :     0.00    Runoff            :    21.15
Irrigation     :     0.00    Transpiration     :    14.60
Bottom flux    :   -66.81    Soil evaporation  :    35.61
                             Crack flux        :     0.00
=========================    ============================
Sum            :    61.18    Sum               :    71.56

Period             :  2008-01-01  until  2008-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       785.96 cm
Initial :       797.56 cm
            =============
Change          -11.59 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   139.59    Interception      :     0.23
Runon          :     0.00    Runoff            :    21.30
Irrigation     :     0.00    Transpiration     :    18.39
Bottom flux    :   -72.58    Soil evaporation  :    38.69
                             Crack flux        :     0.00
=========================    ============================
Sum            :    67.01    Sum               :    78.61

Period             :  2009-01-01  until  2009-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       807.02 cm
Initial :       785.96 cm
            =============
Change           21.06 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   131.24    Interception      :     0.48
Runon          :     0.00    Runoff            :     3.10
Irrigation     :     0.00    Transpiration     :    16.62
Bottom flux    :   -53.87    Soil evaporation  :    36.11
                             Crack flux        :     0.00
=========================    ============================
Sum            :    77.37    Sum               :    56.31

Period             :  2010-01-01  until  2010-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       794.71 cm
Initial :       807.02 cm
            =============
Change          -12.31 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   116.74    Interception      :     0.48
Runon          :     0.00    Runoff            :     8.82
Irrigation     :     0.00    Transpiration     :    20.48
Bottom flux    :   -63.85    Soil evaporation  :    35.43
                             Crack flux        :     0.00
=========================    ============================
Sum            :    52.89    Sum               :    65.20

Period             :  2011-01-01  until  2011-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       795.24 cm
Initial :       794.71 cm
            =============
Change            0.53 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   157.49    Interception      :     0.48
Runon          :     0.00    Runoff            :    29.53
Irrigation     :     0.00    Transpiration     :    20.91
Bottom flux    :   -68.15    Soil evaporation  :    37.89
                             Crack flux        :     0.00
=========================    ============================
Sum            :    89.34    Sum               :    88.81

Period             :  2012-01-01  until  2012-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       792.59 cm
Initial :       795.24 cm
            =============
Change           -2.65 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   124.48    Interception      :     0.35
Runon          :     0.00    Runoff            :     3.33
Irrigation     :     0.00    Transpiration     :    22.69
Bottom flux    :   -66.89    Soil evaporation  :    33.86
                             Crack flux        :     0.00
=========================    ============================
Sum            :    57.59    Sum               :    60.24

Period             :  2013-01-01  until  2013-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       788.43 cm
Initial :       792.59 cm
            =============
Change           -4.17 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   136.24    Interception      :     0.24
Runon          :     0.00    Runoff            :    11.91
Irrigation     :     0.00    Transpiration     :    20.91
Bottom flux    :   -70.58    Soil evaporation  :    36.77
                             Crack flux        :     0.00
=========================    ============================
Sum            :    65.66    Sum               :    69.83

Period             :  2014-01-01  until  2014-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       798.91 cm
Initial :       788.43 cm
            =============
Change           10.48 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :    90.69    Interception      :     0.35
Runon          :     0.00    Runoff            :     1.08
Irrigation     :     0.00    Transpiration     :    20.75
Bottom flux    :   -27.07    Soil evaporation  :    30.96
                             Crack flux        :     0.00
=========================    ============================
Sum            :    63.62    Sum               :    53.14

Period             :  2015-01-01  until  2015-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       807.69 cm
Initial :       798.91 cm
            =============
Change            8.78 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   121.80    Interception      :     0.36
Runon          :     0.00    Runoff            :     1.92
Irrigation     :     0.00    Transpiration     :    15.71
Bottom flux    :   -58.23    Soil evaporation  :    36.80
                             Crack flux        :     0.00
=========================    ============================
Sum            :    63.57    Sum               :    54.79

Period             :  2016-01-01  until  2016-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       799.22 cm
Initial :       807.69 cm
            =============
Change           -8.46 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   142.59    Interception      :     0.39
Runon          :     0.00    Runoff            :    22.55
Irrigation     :     0.00    Transpiration     :    16.76
Bottom flux    :   -69.10    Soil evaporation  :    42.26
                             Crack flux        :     0.00
=========================    ============================
Sum            :    73.49    Sum               :    81.95

Period             :  2017-01-01  until  2017-12-31 
Depth soil profile : 1500.00 cm

            Water storage
Final   :       791.80 cm
Initial :       799.22 cm
            =============
Change           -7.42 cm


Water balance components (cm)

In                           Out
=========================    ============================
Rain + snow    :   132.41    Interception      :     0.23
Runon          :     0.00    Runoff            :    16.35
Irrigation     :     0.00    Transpiration     :    15.34
Bottom flux    :   -68.18    Soil evaporation  :    39.73
                             Crack flux        :     0.00
=========================    ============================
Sum            :    64.23    Sum               :    71.65
