module Damiano
#=
  Module to compute array of h-theta-k-values according to Damiano
=#
include("/home/Wesseling/ISMC/Julia/Types.jl")
using ElasticArrays
using CSV
using DataFrames

# using Types

export compute
export readParams


# physics = ElasticArray{Types.PhysicsPoint}(10000)
type Params
  psiA::Float64
  psiI::Float64
  b::Float64
  d::Float64
  kSat::Float64
  thetaS::Float64
end

#params = Params(10.8, 17.5, 6.94, 0.026, 0.045, 0.451)

function setParams(aPsiA::Float64, aPsiI::Float64, aB::Float64, aD::Float64,aKSat::Float64,aThetaS::Float64)
  # paramFrame = CSV.read("/home/Wesseling/ISMC/Julia/data.csv")
  global params = Params(aPsiA, aPsiI, aB, aD, aKSat, aThetaS)
 #  println(params.kSat)
  global physics = ElasticArray{Types.PhysicsPoint}(10000)
end;

function computeThetaWet(psi::Float64)
#   println("psiA=",params.psiA,"   d=",params.d)
   ratio = psi / params.psiA
   theta = params.thetaS * (1.0 - params.d * ratio * ratio)
   return theta
end

function computePsiWet(theta::Float64)
  psi = 0.1
  arg = (1.0/params.d) * (1.0 - (theta / params.thetaS))
  if arg > 0.0
    psi = params.psiA * sqrt(arg)
  else 
    println("Error for theta =, d= and thetaS=", theta, "   " , params.d, "  ",params.thetaS)
  end
  return psi
end

function computeThetaDry(psi::Float64)
  theta = 1.77 * params.thetaS * ((0.173 / psi)^(1.0/params.b))
  return theta
end

function compute()
  i = 0
  k = 1.0
  dLogPsi = 0.01
  logPsi = -1.0 * dLogPsi
  psi = 10.0^logPsi
  theta = params.thetaS
  while psi < 1.0e6 && theta > 0.01 && k > 1.0e-12 && i < 10000
    if logPsi < 3.0 
      dLogPsi = 0.01
    else
      dLogPsi = 0.1
    end
    logPsi += dLogPsi
    psi = 10.0^logPsi
    if psi > params.psiI
      theta = computeThetaDry(psi)
    else
      theta = computeThetaWet(psi)
    end
 #   println(psi, "   ", theta)
    k = params.kSat * ((theta/params.thetaS)^(2.0*params.b + 3.0))
    i+=1
    physics[i] = Types.PhysicsPoint(theta, psi, k)
  end
  resize!(physics, i)

# correct wet part
  thetaI = computeThetaDry(params.psiI)
  psiIwet = computePsiWet(thetaI)
  f = params.psiI / psiIwet
  physics[1].k *= 100.0
  for j in 2:i
    if physics[j].theta > thetaI
      physics[j].h *= f
    end
    physics[j].h *= 10.1972
    physics[j].k *= 100.0
  end

  return physics
end

end;
