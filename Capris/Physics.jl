module Physics
#=
  Soil physical computations
=#
  include("/home/Wesseling/ISMC/Julia/Damiano.jl")
  include("/home/Wesseling/ISMC/Julia/Types.jl")
  using(ElasticArrays)
  #using(Types)

  export initialize
  export getTheta

  physics=ElasticArray{Types.PhysicsPoint}(100)


  function initialize()
  
    return
  end

  function setParams(aPsiA::Float64, aPsiI::Float64, aB::Float64, aD::Float64, aKSat::Float64, aThetaS::Float64)
    Damiano.setParams(aPsiA, aPsiI, aB, aD, aKSat, aThetaS)
    global physics = Damiano.compute()
    #println(size(physics))
  end;

  function getTheta(h::Float64)
  #  println(h)
    hAbs = abs(h)
    theta = -1.0
    if hAbs < physics[1].h
      theta = physics[1].theta
    else
      hLim = physics[end].h
      if hAbs > hLim
        theta = physics[end].theta
      else
        low = 1
        high = length(physics)
        while high - low > 1
          middle = div(high + low, 2)
          if physics[middle].h > hAbs
            high = middle
          else
            low = middle
          end
        end
        slope = (physics[high].theta - physics[low].theta) / (physics[high].h - physics[low].h)
        theta = physics[low].theta + slope * (hAbs - physics[low].h)
      end;
    end
 #   println(h,"  ",physics[1].h, "  ",physics[end].h, "   ",theta)
    return theta
  end

  function getK(h::Float64)
    k = -1.0
    if h < physics[1].h
      k = physics[1].k
    else
      hLim = physics[end].h
      if h > hLim
        k = physics[end].k
      else
        low = 1
        high = length(physics)
        while high - low > 1
          middle = div(high + low, 2)
          if physics[middle].h > h
            high = middle
          else
            low = middle
          end
        end
        slope = (physics[high].k - physics[low].k) / (physics[high].h - physics[low].h)
        k = physics[low].k + slope * (h - physics[low].h)
      end;
    end
 #   println(h,"   ",k)
    return k
  end

end;
