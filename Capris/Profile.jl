module Profile
#= 
  Calculations for the profile
=#

  include("/home/Wesseling/ISMC/Julia/Types.jl")
  include("/home/Wesseling/ISMC/Julia/Physics.jl")
  using ElasticArrays
  using CSV
  using DataFrames

  export getStorageCapacity

  gwl = 220.0

  function getStorageCapacity(q::Float64)
    soilParams = CSV.read("/home/Wesseling/ISMC/Julia/data.csv")
    boundaryOfLayer = ElasticArray{Float64}(10)
    numberOfLayers = size(soilParams,1)
    resize!(boundaryOfLayer, numberOfLayers)
    z = 0.0
    for i in 1:numberOfLayers
      z+=soilParams[i,1]
      boundaryOfLayer[i] = z
    end;
    if numberOfLayers == 1 || gwl < boundaryOfLayer[1]
      layer = 1
    else
      for i=2:numberOfLayers
        if gwl > boundaryOfLayer[i-1] && gwl <= boundaryOfLayer[i]
          layer = i
        end
      end
    end

    # println("layer=", layer)
    # println(boundaryOfLayer)
    if layer == 1 
      boundaryOfLayer[1] = gwl
    else
      for i in 2:layer
        j = layer - i + 2
      #  println(i, "  ", j)
        boundaryOfLayer[j] = gwl - boundaryOfLayer[j-1]
      end
      boundaryOfLayer[1] = gwl
    end

    # println(boundaryOfLayer)
    # println("Layer=",layer)

    layer += 1
    boundary = -1.0


 #   outputData = ElasticArray{Types.OutputPoint}(17000)
    Physics.initialize()
    z = 0.0
    h = 0.0
    dh = -1.0
    theta = 0.0
    thetaSat = 0.0
    vAir = 0.0

    i = 0
    while h > -16000.0 && z < gwl
      #println(z, "   ", h, "   ", layer, "   ", boundary)
      if z > boundary && layer > 1
        layer = layer - 1
        #println(layer)
        boundary = boundaryOfLayer[layer]
       # println(layer, "   ", boundary)
        Physics.setParams(soilParams[layer,2], soilParams[layer,3], soilParams[layer,4], soilParams[layer,5], soilParams[layer,6], soilParams[layer,7])
        thetaSat = Physics.getTheta(0.0)
      end

      hOld = h
      h = h + dh
      hCenter = 0.5 * (h + hOld)
      k = Physics.getK(abs(hCenter))
      dz = -1.0 * dh / (1.0 + (q / k))
      z += dz
      theta = Physics.getTheta(hCenter)
      vAir += (thetaSat - theta) * dz
      i += 1
  #    outputData[i] = Types.OutputPoint(z, h, theta, thetaSat)
    end

    if z < gwl
      dz = gwl - z
      vAir += (thetaSat - Physics.getTheta(16000.0)) * dz

      i+=1
   #   outputData[i] = Types.OutputPoint(gwl, h, theta, thetaSat)   
    end
   # println("i = ", i)
    #resize!(outputData,i)

 #   df = DataFrame(j=1:i, z=0.0, h=0.0, theta=0.0, thetaSat=0.0)
 #   for j in 1:i
 #     df[j,1] = j
 #     if (outputData[j].z < gwl)
 #       df[j,2] = outputData[j].z
 #     else
 #      df[j,2] = gwl
 #     end
 #     df[j,3] = outputData[j].h
 #     df[j,4] = outputData[j].theta
 #     df[j,5] = outputData[j].thetaSat
 #   end

 #   CSV.write("/home/Wesseling/ISMC/Output/out.csv", df)
     
    return vAir
  end

end;
