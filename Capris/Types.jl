module Types
  export PhysicsPoint
  export OutputPoint

  mutable struct PhysicsPoint
    theta::Float64
    h::Float64
    k::Float64
  end

  mutable struct OutputPoint
    z::Float64
    h::Float64
    theta::Float64
    thetaSat::Float64
  end
end;
