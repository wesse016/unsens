#!/bin/sh
#-----------------------------Job name------------------------------
#SBATCH --job-name="swap_sobol"
#
#-----------------------------Mail----------------------------------
#SBATCH --mail-user=jan.wesseling@wur.nl
#SBATCH --mail-type=END
#
#-----------------------------Output files--------------------------
#SBATCH --output="log/output-%A_%a.out"
#SBATCH --error="log/error-%A_%a.out"
#
#-----------------------------Job info------------------------------
#SBATCH --comment="SWAP runs"
#
#-----------------------------Resources-----------------------------
#SBATCH --partition=ESG_Low
#SBATCH --time=00:30:00
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=2000
#SBATCH --array=<First>-<Last>
#SBATCH --tmp=1000M
#SBATCH --requeue
#SBATCH --constraint=normalmem
#
#--------------------------------------------------------------------
#
echo "run swap computation"
cd /tmp
mkdir wesse016-run${SLURM_ARRAY_TASK_ID}
cd wesse016-run${SLURM_ARRAY_TASK_ID}
cp -r /home/WUR/wesse016/swapData/* .
cp /lustre/scratch/WUR/ESG/wesse016/run${SLURM_ARRAY_TASK_ID}/run.sh .
cp /lustre/scratch/WUR/ESG/wesse016/run${SLURM_ARRAY_TASK_ID}/swap.swp .
cd crops
cp /lustre/scratch/WUR/ESG/wesse016/run${SLURM_ARRAY_TASK_ID}/soyd.crp .
cd ..
sh ./run.sh
cd /lustre/scratch/WUR/ESG/wesse016/run${SLURM_ARRAY_TASK_ID}
cp /tmp/wesse016-run${SLURM_ARRAY_TASK_ID}/result.* .
cp /tmp/wesse016-run${SLURM_ARRAY_TASK_ID}/*.ok .
rm -r /tmp/wesse016-run${SLURM_ARRAY_TASK_ID}
echo "All Done!"
