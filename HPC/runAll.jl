#=
  Procedure to submit jobs on hpc

  Author: Jan G. Wesseling
  Date: Aug. 8, 2018
=#

function countSubstring(where::String, what::String)
  hits = 0
  first = 1
  while true
    location = search(where, what, first)
    isempty(location) && return hits
    hits += 1
    first = location.stop + 1
  end
end

function getJobs()
  jobs = readstring(`squeue -u wesse016`)
  # println(jobs)
  n = countSubstring(jobs, "wesse016")
  # println(n)
  return n
end

function createCommandFile(first::Integer, last::Integer)
  inputFile = "Template.sh"
  outputFile = "runSwap.sh"

  inFil = open(inputFile, "r")
  outFil = open(outputFile, "w")
  while !eof(inFil)
    myLine = readline(inFil)
    newLine1 = replace(myLine, "<First>", first)
    newLine2 = replace(newLine1, "<Last>", last)
    println(outFil, newLine2)
  end
  close(inFil)
  close(outFil)

  return 0
end

function makeRuns(first::Integer, last::Integer)
  status = 0  
  n = 1

  # create file
  createCommandFile(first, last)

  # submit
  run(`sbatch runSwap.sh`)

  # wait for 1 day max and check every 60 seconds
  i = 0
  while i < 1440 && n > 0
    i += 1
    n = getJobs()
    println(n, " jobs running")
    sleep(60)
  end

  if i == 2880
    println("Jobs not finished in time")
    status = 1
  end
end

# main program

println("Start: ", Dates.format(now(), "yyyy-mm-dd:HH:MM:SS"))

runsToMake = 20000
firstRun = 19001
lastRun = firstRun - 1
steps = 1000

while lastRun < runsToMake
  lastRun += steps
  if lastRun > runsToMake
    lastRun = runsToMake
  end
 
  println("Starting jobs ", firstRun, " to ", lastRun)
  status = makeRuns(firstRun, lastRun)
  if status == 1
    lastRun = runsToMake
  end
    
  firstRun += steps
end

println("Finish: ", Dates.format(now(), "yyyy-mm-dd:HH:MM:SS"))


