#=
  Program to read the output files of a Sobol analysis and
  store them is a csv-file
  
  Author : Jan G. Wesseling
  Date   : March 11th, 2019
=#

  using CSV, DataFrames
  mat = Array{String,2}(undef,11,9)
  mat[1,1] = ""
  mat[2,1] = "ThetaRA"
  mat[3,1] = "ThetaSA"
  mat[4,1] = "alfaA"
  mat[5,1] = "nA"
  mat[6,1] = "kSA"
  mat[7,1] = "ThetaRB"
  mat[8,1] = "ThetaSB"
  mat[9,1] = "alfaB"
  mat[10,1] = "nB"
  mat[11,1] = "kSB"

  baseName = "/home/wesseling/Sobol/Data/SobolOut_"
  for k in 1:9
    fileName = baseName * string(k) * ".txt"
    println(fileName)

    f = open(fileName)

    reading = true
    while !eof(f) & reading
      lineRead = readline(f)
      if occursin("Total indices", lineRead)

        for j = 1:8
          lineRead = readline(f)
          parts = split(lineRead, " ")
          mat[1, j + 1] = parts[3]

          # skip 2 lines
          lineRead = readline(f)
          lineRead = readline(f)

          # read 10 lines
          for i = 1:10
            lineRead = readline(f)
            parts = split(lineRead, " ", keepempty=false)
            mat[i+1, j+1] = parts[2]
          end
       
          # skip 1 line
          if (j < 8)
            lineRead = readline(f)
          end
        end
        reading = false
      end
    end
    close(f)

    outputFile = "/home/wesseling/Sobol/Data/table_" * string(k) * ".csv"
    io = open(outputFile, "w")
    CSV.write(io, DataFrame(mat), delim=",")
    close(io)
  end

